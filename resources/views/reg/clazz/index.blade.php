@extends('layouts.app')
@section('content')
<div class="content-holder">
    <div class="row">
        <div class="col s12">
            <h5>Classes <a href="{{route('add-class')}}" class="waves-effect waves-light btn btn-small right">+ New </a></h5>
            <div class="divider"></div>
        </div>
    </div>
</div>


<div class="row">
    <div class="col s12">
        <table class="four-row-table mdl-data-table">
            <thead>
                <tr>
                    <th style="widht: 1%">#</th>
                    <th>Name</th>
                    <th>Short Name</th>
                    <th>Streams</th>
                    <th>Actions</th>
                </tr>
            </thead>

            <tbody>
                @foreach ($clazzs as $key => $class)
                <tr>
                    <td>{{++$key}}</td>
                    <td>{{$class->name}}</td>
                    <td>{{$class->short_name}}</td>
                    <td>{{$class->streams->count()}}</td>
                    <td>
                        <a href="{{url("add-class/".hashEncode($class->id))}}" class="waves-effect waves-light btn btn-small">Update</a>
                        <a class="waves-effect waves-light btn btn-small">Delete</a>
                        <a href="{{url("streams/".hashEncode($class->id))}}" class="waves-effect waves-light btn btn-small">Streams</a>
                        <a href="#!" @click="showSubjects('{{$class->id}}', '{{url("streams/".hashEncode($class->id))}}')" class="waves-effect waves-light btn btn-small">Subjects</a>
                    </td>
                </tr>
                @endforeach
            </tbody>
        </table>
    </div>
</div>
@include('reg.clazz.modal.assign_class_subjects')
@endsection