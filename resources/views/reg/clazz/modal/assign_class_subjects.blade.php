 <!-- Modal Structure -->
 <div id="modal-class-subjects" class="modal modal-fixed-footer">
     <div class="modal-content">
         <table class="striped bordered">
             <thead>
                 <tr>
                     <th>Subject Name</th>
                     <th>Particular</th>
                 </tr>
             </thead>
             <tbody>
                 <template v-for='(item, index) in allSubjects'>
                     <tr>
                         <td v-if="item.particulars && item.particulars.length > 0" :rowspan="item.particulars.length">
                             @{{item.name}}</td>
                         <td v-if="item.particulars && item.particulars.length == 0" colspan="2">
                             <label>
                                 <input type="checkbox"  :checked="onlySubjects.includes(item.id)" :data-subject-name="item.name" :data-subject-id="item.id"
                                     class="filled-in is-subject" />
                                 <span>@{{item.name}}</span>
                             </label>
                         </td>
                         <td v-if="item.particulars && item.particulars.length > 0">
                             <label>
                                 <input type="checkbox" :checked="onlySubjectPats.includes(item.particulars[0].id)" :data-subject-name="item.name" :data-subject-id="item.id"
                                     :data-subject-pat-name="item.particulars[0].name"
                                     :data-subject-pat-id="item.particulars[0].id" class="filled-in is-subject" />
                                 <span> @{{item.particulars[0].name}}</span>
                             </label>
                         </td>
                     </tr>
                     <template v-if="item.particulars && item.particulars.length > 1">
                         <tr v-for="(item2, index2) in item.particulars.slice(1)">
                             <td>
                                 <label>
                                     <input type="checkbox" :checked="onlySubjectPats.includes(item2.id)" :data-subject-name="item.name" :data-subject-id="item.id"
                                         :data-subject-pat-name="item2.name"
                                         :data-subject-pat-id="item2.id" class="filled-in is-subject" />
                                     <span>@{{item2.name}}</span>
                                 </label>
                             </td>
                         </tr>
                     </template>
                 </template>
             </tbody>
         </table>
     </div>
     <div class="modal-footer">
         <a href="#!" @click="saveSubjects" class="waves-effect waves-green btn-flat">Save</a>
         <a href="#!" class="modal-close waves-effect waves-green btn-flat">Cancel</a>
     </div>
 </div>