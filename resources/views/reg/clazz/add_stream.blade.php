@extends('layouts.app')
@section('content')
<div class="content-holder">
    <div class="row">
        <div class="col s12">
                <h5> <a href="{{ url()->previous()}}"> Streams </a>/ Streams Details</h5>
            <div class="divider"></div>
        </div>
    </div>
</div>

<div class="row">
    <div class="col s12 ">
        <div class="container">
            <div class="card">
                <div class="card-content">
                    <form  class="form-validate" action="{{route('save-stream')}}" method="post">
                        {{ csrf_field() }}
                        <input name="id" value="{{!isset($stream) ?'': $stream->id}}" hidden />
                        <input name="clazz_id" value="{{!isset($clazz) ?'': $clazz->id}}" hidden />

                        <div class="row">
                            <div class="input-field col s6">
                                <input id="name" name="name" type="text" value="{{!isset($stream) ?'': $stream->name}}" class="validate" required>
                                <label for="name">Name</label>
                                <div class="error-holder"></div>
                            </div>
                            <div class="input-field col s6">
                                <input id="short-name" name="short_name" value="{{!isset($stream) ?'': $stream->short_name}}" type="text" class="validate" required>
                                <label for="short-name">Short Name</label>
                                <div class="error-holder"></div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col s12">
                                <button type="submit" class="waves-effect waves-light btn right">Save</button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection