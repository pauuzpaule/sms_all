@extends('layouts.app')
@section('content')
<div class="content-holder">
    <div class="row">
        <div class="col s12">
            <h5> <a href="{{route('classes')}}">Classes</a> /Streams [{{$clazz->name}}] <a href="{{url('add-stream/'.hashEncode($clazz->id))}}" 
                class="waves-effect waves-light btn btn-small right">+ New </a></h5>
            <div class="divider"></div>
        </div>
    </div>
</div>


<div class="row">
    <div class="col s12">
        <table class="three-row-table mdl-data-table">
            <thead>
                <tr>
                    <th style="widht: 1%">#</th>
                    <th>Name</th>
                    <th>Short Name</th>
                    <th>Actions</th>
                </tr>
            </thead>

            <tbody>
                
                @foreach ($streams as $key => $stream)
                <tr>
                    <td>{{++$key}}</td>
                    <td>{{$stream->name}}</td>
                    <td>{{$stream->short_name}}</td>
                    <td>
                        <a href="{{url('add-stream/'.hashEncode($clazz->id).'/'.$stream->id)}}" class="waves-effect waves-light btn btn-small">Update</a>
                        <a @click="showSubjects('{{$clazz->id}}', '#', '{{$stream->id}}')" class="waves-effect waves-light btn btn-small">Subjects</a>
                        <a class="waves-effect waves-light btn btn-small">Delete</a>
                    </td>
                </tr>
                @endforeach
            </tbody>
        </table>
    </div>
</div>

@include('reg.clazz.modal.assign_class_subjects')

@endsection