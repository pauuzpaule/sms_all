@extends('layouts.app')
@section('content')
<div class="content-holder">
    <div class="row">
        <div class="col s12">
            <h5 class="center-align">Particulars</h5>
            <div class="divider"></div>
        </div>
    </div>
</div>

<div class="row">
    <div class="col s12 ">
        <div class="container">
            <div class="card">
                <div class="card-content">
                    <form action="{{route('save-particular')}}" method="post">
                        <input type="hidden" name="subject_id" value="{{$id}}">
                        <input type="hidden" name="id" value="{{isset($subjectParticular) ? $subjectParticular->id: ''}}">
                        {{ csrf_field() }}
                        <div class="row">
                            <div class="input-field col s6">
                                <input id="name" type="text" name="name" class="validate" value="{{isset($subjectParticular) ? $subjectParticular->name: ''}}">
                                <label for="name">Name</label>
                            </div>
                            <div class="input-field col s6">
                                <input id="short-name" type="text" name="short_name" class="validate" value="{{isset($subjectParticular) ? $subjectParticular->short_name: ''}}">
                                <label for="short-name">Short Name</label>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col s12">
                                <button type="submit" class="waves-effect waves-light btn right">Save</button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection