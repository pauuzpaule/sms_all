@extends('layouts.app')
@section('content')
<div class="content-holder">
    <div class="row">
        <div class="col s12">
                <h5>Subjects <a href="{{route('add-subject')}}" class="waves-effect waves-light btn btn-small right">+ New </a></h5>
            <div class="divider"></div>
        </div>
    </div>
</div>


<div class="row">
    <div class="col s12">
        <table class="three-row-table mdl-data-table">
            <thead>
                <tr>
                    <th style="widht: 1%">#</th>
                    <th>Name</th>
                    <th>Short Name</th>
                    <th>Actions</th>
                </tr>
            </thead>

            <tbody>
                @foreach ($subjects as $key => $subject)
                    <tr>
                        <td>{{++$key}}</td>
                        <td>{{$subject->name}}</td>
                        <td>{{$subject->short_name}}</td>
                        <td>
                            <a href="{{url("add-subject/".hashEncode($subject->id))}}" class="waves-effect waves-light btn">Update</a>
                            <a href="#!" class="waves-effect waves-light btn">Delete</a>
                            <a href="{{url("particulars/".hashEncode($subject->id))}}" class="waves-effect waves-light btn">Particulars</a>
                        </td>
                    </tr>    
                @endforeach
            </tbody>
        </table>
    </div>
</div>



@endsection