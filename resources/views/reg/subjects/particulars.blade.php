@extends('layouts.app')
@section('content')
<div class="content-holder">
    <div class="row">
        <div class="col s12">
            <h5> <a href="{{route('subjects')}}">Subjects</a> /Particulars [{{$subject->name}}]  <a href="{{url('add-particular/'.hashEncode($subject->id))}}"  
                class="waves-effect waves-light btn btn-small right">+ New </a></h5>
            <div class="divider"></div>
        </div>
    </div>
</div>


<div class="row">
    <div class="col s12">
        <table class="four-row-table mdl-data-table">
            <thead>
                <tr>
                    <th style="widht: 1%">#</th>
                    <th>Name</th>
                    <th>Short Name</th>
                    <th>Code</th>
                    <th>Actions</th>
                </tr>
            </thead>

            <tbody>
                @foreach ($particulars as $key => $particular)
                    <tr>
                        <td>{{++$key}}</td>
                        <td>{{$particular->name}}</td>
                        <td>{{$particular->short_name}}</td>
                        <td>{{$particular->code}}</td>
                        <td>
                            <a class="waves-effect waves-light btn">Update</a>
                            <a class="waves-effect waves-light btn">Delete</a>
                        </td>
                    </tr>
                @endforeach
            </tbody>
        </table>
    </div>
</div>


@endsection