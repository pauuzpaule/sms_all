@extends('layouts.app')
@section('content')
<div class="content-holder">
    <div class="row">
        <div class="col s12">
            <h5>  <a href="{{route("subjects")}}">Subjects </a>/subject Details</h5>
            <div class="divider"></div>
        </div>
    </div>
</div>

<div class="row">
    <div class="col s12 ">
        <div class="container">
            <div class="card">
                <div class="card-content">
                    <form class="form-validate" action="{{route("save-subject")}}" method="post"> 
                        <input type="hidden" name="id" value="{{!isset($subject) ?'': $subject->id}}">
                        {{ csrf_field() }}
                        <div class="row">
                            <div class="input-field col s6">
                                <input id="name" type="text" name="name" value="{{!isset($subject) ?'': $subject->name}}" class="validate" required>
                                <label for="name">Name</label>
                                <div class="error-holder"></div>
                            </div>
                            <div class="input-field col s6">
                                <input id="short-name" type="text" name="short_name" value="{{!isset($subject) ?'': $subject->short_name}}" class="validate" required>
                                <label for="short-name">Short Name</label>
                                <div class="error-holder"></div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col s12">
                                <button type="submit" class="waves-effect waves-light btn right">Save</button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection