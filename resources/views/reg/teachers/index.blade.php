@extends('layouts.app')
@section('content')
<div class="content-holder">
    <div class="row">
        <div class="col s12">
                <h5>Teacher <a href="{{route("add-teacher")}}" class="waves-effect waves-light btn btn-small right">+ New </a></h5>
            <div class="divider"></div>
        </div>
    </div>
</div>

<div class="row">
    <div class="col s12">
        <table class="three-row-table mdl-data-table">
            <thead>
                <tr>
                    <th style="widht: 1%">#</th>
                    <th>Name</th>
                    <th>Email</th>
                    <th></th>
                </tr>
            </thead>

            <tbody>
                @foreach ($users as $key => $user)
                    <tr>
                        <td>{{++$key}}</td>
                        <td>{{$user->name}}</td>
                        <td>{{$user->email}}</td>
                        <td>
                            <a @click="makeClassTeacher('{{$user->id}}', '{{$user->name}}')" class="waves-effect waves-light btn btn-small">Assign Class</a>
                            <a href="{{url("add-teacher-subjects/".hashEncode($user->id))}}"
                                 class="waves-effect waves-light btn btn-small">Assign Subjects</a>
                        </td>
                    </tr>
                @endforeach
            </tbody>
        </table>
    </div>
</div>
@include('reg.teachers.modal.make_class_teacher')
@endsection