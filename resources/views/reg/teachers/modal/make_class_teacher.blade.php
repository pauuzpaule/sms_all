<div id="modal-class-teacher" class="modal modal-fixed-footer">
    <div class="modal-content">
        <div class="row">
            <div class="col s12">
                <h5>Make: [@{{selected_teacher_name}}]</h5>
                <form id="form-make-class-teacher" class="form-validate" action="make-class-teacher" method="post">
                    {{ csrf_field() }} 
                    <input hidden id="user_id" name="user_id" :value="selected_teacher_id">
                    <class-teacher-subjects></class-teacher-subjects>
                </form>
            </div>
        </div>
        
    </div>
    <div class="modal-footer">
        <a href="#!" @click="submitMakeClassTeacherForm" class="waves-effect waves-green btn-flat">Save</a>
        <a href="#!" class="modal-close waves-effect waves-green btn-flat">Cancel</a>
    </div>
</div>