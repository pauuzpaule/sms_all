@extends('layouts.app')
@section('content')
<div class="content-holder">

    <div class="row">
        <div class="col s12">
            <h5><a href="{{route('teachers')}}">Teachers</a> /Teacher [{{$teacher->name}}]</h5>
            <div class="divider"></div>
        </div>
    </div>
    <class-subjects :teacher="{{$teacher}}"></class-subjects>
</div>


@endsection