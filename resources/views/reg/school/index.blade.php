@extends('layouts.app')
@section('content')
<div class="content-holder">
    <div class="row">
        <div class="col s12">
            <h5> <a href="{{route('school')}}">School </a>/ School Details</h5>
            <div class="divider"></div>
        </div>
    </div>
</div>

<div class="row">
    <div class="col s12 ">
        <div class="container">
            <div class="card">
                <div class="card-content">

                    @if (session('schoolMessage'))
                        <div class="error">
                            {{ session('schoolMessage') }}
                        </div>
                    @endif

                    <form id="form-add-school" class="form-validate" action="{{route('save-school')}}"
                        enctype="multipart/form-data" method="post">
                        <input type="hidden" name="id" value="{{!isset($school) ?'': $school->id}}">
                        {{ csrf_field() }}
                        <div class="row">
                            <div class="input-field col s6">
                                <input id="name" type="text" name="name" value="{{!isset($school) ?'': $school->name}}"
                                    class="validate" required>
                                <label for="name">Name</label>
                                <div class="error-holder"></div>
                            </div>
                            <div class="input-field col s6">
                                <input id="email" type="email" name="email"
                                    value="{{!isset($school) ?'': $school->email}}" class="validate" required>
                                <label for="email">Email</label>
                                <div class="error-holder"></div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="input-field col s6">
                                <input id="address" type="text" name="address"
                                    value="{{!isset($school) ?'': $school->address}}" class="validate" required>
                                <label for="address">Address</label>
                                <div class="error-holder"></div>
                            </div>
                            <div class="input-field col s6">
                                <input id="phone" type="text" name="phone" value="{{!isset($school) ?'': $school->phone}}"
                                    class="validate" required>
                                <label for="phone">Phone</label>
                                <div class="error-holder"></div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="input-field col s6">
                                <input id="motto" type="text" name="motto" value="{{!isset($school) ?'': $school->motto}}"
                                    class="validate" required>
                                <label for="motto">Motto</label>
                                <div class="error-holder"></div>
                            </div>
                            <div class="input-field col s6">
                                <input id="website" type="text" name="website" class="validate"
                                    value="{{!isset($school) ?'': $school->website}}" required>
                                <label for="website">Website</label>
                                <div class="error-holder"></div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col s12">
                                <div class="file-field input-field">
                                    <div class="btn">
                                        <span>School Badge</span>
                                        <input type="file" name="badge" title="Image for the badge is required"
                                            required>
                                    </div>
                                    <div class="file-path-wrapper">
                                        <input class="file-path validate" type="text">
                                    </div>
                                    <div class="error-holder"></div>
                                </div>

                            </div>
                        </div>
                        <div class="row">
                            <div class="col s12">
                                <button type="submit" class="waves-effect waves-light btn right">Save</button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection