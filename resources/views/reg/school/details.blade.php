@extends('layouts.app')
@section('content')
<div class="content-holder">
    <div class="row">
        <div class="col s12">
            <h5> School</h5>
            <div class="divider"></div>
        </div>
    </div>
</div>

<div class="row">
    <div class="col s12 ">
        <div class="container">
            <div class="card">
                <div class="card-content">
                    <table>
                        <tbody>
                            <tr>
                                <td style="text-align: center;" colspan="2">
                                    <img src="{{asset("storage/{$school->badge}")}}">
                                </td>
                            </tr>
                            <tr>
                                <td>Name: </td>
                                <td>{{$school->name}}</td>
                            </tr>
                            <tr>
                                <td>Email: </td>
                                <td>{{$school->email}}</td>
                            </tr>
                            <tr>
                                <td>Motto: </td>
                                    <td>{{$school->motto}}</td>
                            </tr>
                            <tr>
                                <td>Phone: </td>
                                <td>{{$school->phone}}</td>
                            </tr>
                            <tr>
                                <td>Website: </td>
                                <td>{{$school->website}}</td>
                            </tr>
                        </tbody>
                    </table>

                    <div class="row mt">
                        <div class="col s12 right">
                                <a href="{{asset("school-details")}}" class="waves-effect waves-light btn right">+
                                        Add/Modify</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection