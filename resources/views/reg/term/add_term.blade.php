@extends('layouts.app')
@section('content')
<div class="content-holder">
    <div class="row">
        <div class="col s12">
            <h5> <a href="{{route('terms')}}">Terms </a>/ Term Details</h5>
            <div class="divider"></div>
        </div>
    </div>
</div>

<div class="row">
    <div class="col s12 ">
        <div class="container">
            <div class="card">
                <div class="card-content">
                    <form id="form-add-school" class="form-validate" action="{{route('save-term')}}" method="post">
                        <input type="hidden" name="id" value="{{!isset($term) ?'': $term->id}}">
                        {{ csrf_field() }}
                        <div class="row">
                            <div class="input-field col s12">
                                <input id="name" type="text" name="name" value="{{!isset($term) ?'': $term->name}}"
                                    class="validate" required>
                                <label for="name">Name</label>
                                <div class="error-holder"></div>
                            </div>
                        </div>

                        <div class="row">
                            <div class="input-field col s6">
                                <input id="start" type="text" name="start" class="datepicker-any" value="{{!isset($term) ?'': $term->start}}" required>
                                <label for="dob">Start</label>
                                <div class="error-holder"></div>
                            </div>
                            <div class="input-field col s6">
                                <input id="end" type="text" name="end" class="datepicker-any" value="{{!isset($term) ?'': $term->end}}" required>
                                <label for="dob">End</label>
                                <div class="error-holder"></div>
                            </div>
                        </div>
                        
                          <div class="row">
                            <div class="col s12">
                                <button type="submit" class="waves-effect waves-light btn right">Save</button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection