@extends('layouts.app')
@section('content')
<div class="content-holder">
    <div class="row">
        <div class="col s12">
            <h5>Terms <a href="{{route('add-term')}}" class="waves-effect waves-light btn btn-small right">+ New </a>
            </h5>
            <div class="divider"></div>
        </div>
    </div>
</div>


<div class="row">
    <div class="col s12">

        @if (session('termMessage'))
            <div class="error pl-3">
                {{ session('termMessage') }}
            </div>
        @endif

        <table class="data-table mdl-data-table">
            <thead>
                <tr>
                    <th style="widht: 1%">#</th>
                    <th>Name</th>
                    <th>Start</th>
                    <th>End</th>
                    <th>Status</th>
                    <th></th>
                </tr>
            </thead>

            <tbody>
                @foreach ($terms as $key => $term)
                <tr>
                    <td>{{++$key}}</td>
                    <td>{{$term->name}}</td>
                    <td>{{$term->start}}</td>
                    <td>{{$term->end}}</td>
                    <td>{{$term->status}}</td>
                    <td>
                        <a href="{{url("add-term/".hashEncode($term->id))}}"
                            class="waves-effect waves-light btn btn-small">Update</a>
                        <a href="{{route('activate-term', ['term' => hashEncode($term->id)])}}"
                            class="waves-effect waves-light btn btn-small">Activate</a>
                    </td>
                </tr>
                @endforeach
            </tbody>
        </table>
    </div>
</div>


@endsection