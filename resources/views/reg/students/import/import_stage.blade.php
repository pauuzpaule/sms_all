@extends('layouts.app')
@section('content')
<div class="content-holder">
    <div class="row">
        <div class="col s12">
            <h5>Student Details
                <a href="{{ url('/students')}}" class="waves-effect waves-light btn btn-small right"><i
                        class="material-icons left">arrow_left</i> Back
                </a>
                <a href="{{route("confirm-import-students")}}" class="waves-effect waves-light btn btn-small right mr">Save Import</a>
            </h5>
            <div class="divider"></div>
        </div>
    </div>

    <div class="row">
        <div class="col s12 ">
            <table class="highlight">
                <thead>
                    <tr>
                        <th style="widht: 1%">#</th>
                        <th>Name</th>
                        <th>Dob</th>
                        <th>Gender</th>
                        <th>Year</th>
                        <th>Guardian name</th>
                        <th>Guardian Contact</th>
                        <th></th>
                    </tr>
                </thead>

                <tbody>
                    @foreach ($students as $key => $student)
                    <tr>
                        <td>{{++$key}}</td>
                        <td class="{{isset($student->studentType) ?: 'error'}}">{{$student->name}}</td>
                        <td class="{{isDobValid($student->dob) ?: 'error'}}">{{$student->dob}}</td>
                        <td class="{{isGenderValid($student->gender) ?: 'error'}}">{{$student->gender}}</td>
                        <td class="{{isYearValid($student->year_of_admin) ?: 'error'}}">{{$student->year_of_admin}}</td>
                        <td>{{$student->guardian_name}}</td>
                        <td>{{$student->guardian_contact}}</td>
                        <td>
                            <a href="{{url("view-import-update-students/{$student->id}")}}"
                                class="waves-effect waves-light btn btn-small">Update</a>
                        </td>
                    </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
    </div>
    <update-stage-student></update-stage-student>
</div>


@endsection