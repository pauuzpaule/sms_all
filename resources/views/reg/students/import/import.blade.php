@extends('layouts.app')
@section('content')
<div class="content-holder">
    <div class="row">
        <div class="col s12">
                <h5>Student Details <a href="{{ url()->previous()}}"
                        class="waves-effect waves-light btn btn-small right"><i class="material-icons left">arrow_left</i> Back
                    </a></h5>
            <div class="divider"></div>
        </div>
    </div>
</div>

<div class="row">
    <div class="col s12 ">
        <div class="container">
            <div class="card">
                <div class="card-content">
                    @if ($errors->any())
                        <div class="alert alert-danger">
                            <ul>
                                @foreach ($errors->all() as $error)
                                    <li class="error">{{ $error }}</li>
                                @endforeach
                            </ul>
                        </div>
                    @endif
                    <form action="{{route("import-students")}}" class="form-validate"  enctype="multipart/form-data" method="post">
                        {{ csrf_field() }}
                        <class-select size="s12"></class-select>
                        <div class="row">
                                <div class="col s12">
                                    <div class="file-field input-field">
                                        <div class="btn">
                                            <span>Excel File</span>
                                            <input type="file" name="excel_file" title="Please add file to import" required>
                                        </div>
                                        <div class="file-path-wrapper">
                                            <input class="file-path validate" type="text">
                                        </div>
                                        <div class="error-holder"></div>
                                    </div>
                                </div>
                            </div>
                        <div class="row">
                            <div class="col s12">
                                <button type="submit" class="waves-effect waves-light btn right">Import Students</button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection