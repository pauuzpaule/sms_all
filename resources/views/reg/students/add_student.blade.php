@extends('layouts.app')
@section('content')
    <div class="content-holder">
        <div class="row">
            <div class="col s12">
                <h5>
                    @if (isset($student))
                        <a href="{{url()->previous()}}">Students </a>/ Student Details
                    @else
                        <a href="{{route("students")}}">Students </a>/ Student Details
                    @endif

                </h5>
                <div class="divider"></div>
            </div>
        </div>
    </div>

    <div class="row">
        <div class="col s12 ">
            <div class="container">
                <div class="card">
                    <div class="card-content">
                        <form action="{{route('save-student')}}" class="form-validate" enctype="multipart/form-data"
                              method="post">
                            {{ csrf_field() }}
                            <input hidden name="id" value="{{!isset($student) ?'': $student->id}}">
                            <div class="row center-align">
                                @if (isset($student) && $student->image != null)
                                    <image-picker id="student-image"
                                                  originalimage='{{asset("storage/{$student->image}")}}'
                                                  name="image"></image-picker>
                                @else
                                    <image-picker id="student-image" name="image"></image-picker>
                                @endif

                            </div>
                            <div class="row">
                                <div class="input-field col s6">
                                    <input id="name" type="text" class="validate"
                                           value="{{!isset($student) ?'': $student->name}}" name="name" required>
                                    <label for="name">Name</label>
                                    <div class="error-holder"></div>
                                </div>
                                <div class="input-field col s6">
                                    <input id="dob" type="text" name="dob" class="datepicker dob"
                                           value="{{!isset($student) ?'': $student->dob}}" required>
                                    <label for="dob">Date of Birth</label>
                                    <div class="error-holder"></div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="input-field col s6">
                                    <div class="input-field col s12">
                                        <select name="gender" class="validate" required>
                                            <option value="" disabled {{!isset($student) ? 'selected' : ''}} >Choose Gender
                                            </option>
                                            <option {{isset($student) && $student->gender == 'M' ? 'selected': ''}} value="M">MALE
                                            </option>
                                            <option {{isset($student) && $student->gender == 'F' ? 'selected': ''}}value="F">FEMALE
                                            </option>
                                        </select>
                                        <label>Gender</label>
                                        <div class="error-holder"></div>
                                    </div>
                                </div>
                                <div style="margin-top: 28px" class="input-field col s6">
                                    <select name="year_of_admin" class="validate" required>
                                        @if (isset($student))
                                            <option value="{{$student->year_of_admin}}">{{$student->year_of_admin}}</option>
                                        @endif
                                        @foreach (range(now()->year, 1980) as $year)
                                            <option value="{{$year}}">{{$year}}</option>
                                        @endforeach
                                    </select>
                                    <label for="year_of_admin ">Year of Admission</label>
                                    <div class="error-holder"></div>
                                </div>
                            </div>

                            @if (!isset($student))
                                <class-select size="s12"></class-select>
                            @endif

                            <div class="row">
                                <div class="input-field col s12">
                                    <select name="student_type_id" class="validate" required>
                                        @if (isset($student->studentType))
                                            <option value="{{$student->studentType_id}}">{{$student->studentType->name}}</option>
                                        @endif
                                        @foreach ($studentTypes as $studentType)
                                            <option value="{{$studentType->id}}">{{$studentType->name}}</option>
                                        @endforeach
                                    </select>
                                    <label for="year_of_admin ">Student Type</label>
                                    <div class="error-holder"></div>
                                </div>
                            </div>


                            <div class="row">
                                <div class="input-field col s6">
                                    <input id="guardian_name" type="text" class="validate"
                                           value="{{!isset($student) ?'': $student->guardian_name}}"
                                           name="guardian_name" required>
                                    <label for="guardian_name">Guardian name</label>
                                    <div class="error-holder"></div>
                                </div>
                                <div class="input-field col s6">
                                    <input id="guardian_contact" type="text" class="validate"
                                           value="{{!isset($student) ?'': $student->guardian_contact}}"
                                           name="guardian_contact" required>
                                    <label for="guardian_contact">Guardian contact</label>
                                    <div class="error-holder"></div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col s12">
                                    <button type="submit" class="waves-effect waves-light btn right">Save</button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection