@extends('layouts.app')
@section('content')
<div class="content-holder">
    <div class="row">
        <div class="col s12">
            <h5>Students [{{(!isset($clazz) ?'': $clazz->name)}} {{(!isset($stream) ?'': $stream->name)}}]
                <a href="{{route("view-import-students")}}" class="waves-effect waves-light btn btn-small right">Import
                    Students</a>
                <a href="{{route("add-student")}}" class="waves-effect waves-light btn btn-small right mr">+ New </a>
            </h5>
            <div class="divider"></div>
        </div>
    </div>

    <div class="row">
        <div class="col s3">
            <form class="form-validate" action="{{route("students")}}" method="get">
                <class-select></class-select>
                <div class="row">
                    <div class="col s12">
                        @if (isset($clazz))
                        <?php
                               $link = (isset($stream)) ? url("export-students/{$clazz->id}/{$stream->id}") :  url("export-students/{$clazz->id}");
                             ?>
                        <a href="{{$link}}" class="waves-effect waves-light btn right">Export</a>
                        @endif
                        <button type="submit" class="waves-effect waves-light btn right mr">View</button>
                    </div>
                </div>
            </form>
        </div>
        <div class="col s8 offset-s1">
            <table class="three-row-table mdl-data-table">
                <thead>
                    <tr>
                        <th style="widht: 1%">#</th>
                        <th>Name</th>
                        <th>Student No</th>
                        <th></th>
                    </tr>
                </thead>

                <tbody>
                    @foreach ($students as $key => $student)
                    <tr>
                        <td>{{++$key}}</td>
                        <td>{{$student->name}}</td>
                        <td>{{$student->unique_no}}</td>
                        <td>
                            <a href="{{url("add-student/".hashEncode($student->id))}}"
                                class="waves-effect waves-light btn btn-small">Update</a>
                        </td>
                    </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
    </div>

</div>

@endsection