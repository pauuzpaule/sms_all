@extends('layouts.app')
@section('content')
<div class="content-holder">
    <div class="row">
        <div class="col s12">
            <h5>Users <a href="{{route('add-user')}}" class="waves-effect waves-light btn btn-small right">+ New </a>
            </h5>
            <div class="divider"></div>
        </div>
    </div>
</div>


<div class="row">
    <div class="col s12">
        <table class="data-table mdl-data-table">
            <thead>
                <tr>
                    <th style="widht: 1%">#</th>
                    <th>Name</th>
                    <th>Email</th>
                    <th>user type</th>
                    <th>Last Modified</th>
                    <th></th>
                </tr>
            </thead>

            <tbody>
                @foreach ($users as $key => $user)
                <tr>
                    <td>{{++$key}}</td>
                    <td>{{$user->name}}</td>
                    <td>{{$user->email}}</td>
                    <td>{{$user->type}}</td>
                    <td>{{$user->updated_at}}</td>
                    <td>
                        <a href="{{url("add-user/".hashEncode($user->id))}}"
                            class="waves-effect waves-light btn btn-small">Update</a>
                    </td>
                </tr>
                @endforeach
            </tbody>
        </table>
    </div>
</div>
@endsection