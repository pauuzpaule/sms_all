@extends('layouts.app')
@section('content')
<div class="content-holder">
    <div class="row">
        <div class="col s12">
                <h5> 
                    @if (isset($addingTeacher))
                        <a href="{{route("teachers")}}">Teachers</a>/ Teacher Details
                    @else
                        <a href="{{route("users")}}">Users</a>/ User Details
                    @endif
                   
                 </h5>
            <div class="divider"></div>
        </div>
    </div>
</div>

<div class="row">
    <div class="col s12 ">
        <div class="container">
            <div class="card">
                <div class="card-content">
                    <form action="{{route('save-user')}}" class="form-validate"  enctype="multipart/form-data" method="post">
                        {{ csrf_field() }}
                        <input hidden name="id" value="{{!isset($user) ?'': $user->id}}">
                        <div class="row">
                            <div class="input-field col s6">
                                <input id="name" type="text" class="validate" value="{{!isset($user) ?'': $user->name}}" name="name" required>
                                <label for="name">Name</label>
                                <div class="error-holder"></div>
                            </div>
                            <div class="input-field col s6">
                                <input id="email" type="email" class="validate" value="{{!isset($user) ?'': $user->email}}" name="email" required>
                                <label for="email">Email</label>
                                <div class="error-holder"></div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="input-field col s6">
                                <div class="input-field col s12">
                                    <select name="type" class="validate" required>
                                        @if (isset($addingTeacher))
                                        <option selected value="TEACHER">Teacher</option>
                                        @else
                                            <option value="" disabled {{!isset($user) ? 'selected' : ''}} >Choose your option</option>
                                            <option {{isset($user) && $user->type == 'ADMIN' ? 'selected': ''}} value="ADMIN">Administrator</option>
                                            <option {{isset($user) && $user->type == 'TEACHER' ? 'selected': ''}} value="TEACHER">Teacher</option>
                                            <option {{isset($user) && $user->type == 'FINANCE' ? 'selected': ''}} value="FINANCE">Finance</option>
                                        @endif
                                        
                                    </select>
                                    <label>USER TYPE</label>
                                    <div class="error-holder"></div>
                                </div>
                               
                            </div>
                            <div class="input-field col s6">
                                <div class="input-field col s12">
                                    <select name="gender" class="validate" required>
                                        <option value="" disabled {{!isset($user) ? 'selected' : ''}} >Choose your option</option>
                                        <option {{isset($user) && $user->gender == 'M' ? 'selected': ''}} value="M">MALE</option>
                                        <option {{isset($user) && $user->gender == 'F' ? 'selected': ''}} value="F">FEMALE</option>
                                    </select>
                                    <label>Gender</label>
                                    <div class="error-holder"></div>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col s12">
                                <div class="file-field input-field">
                                    <div class="btn">
                                        <span>Image</span>
                                        <input type="file" name="image">
                                    </div>
                                    <div class="file-path-wrapper">
                                        <input class="file-path validate" type="text">
                                    </div>
                                    <div class="error-holder"></div>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col s12">
                                <button type="submit" class="waves-effect waves-light btn right">Save</button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection