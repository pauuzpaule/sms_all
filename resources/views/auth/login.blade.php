@extends('layouts.guest')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col s6 offset-s3 login-form mt-3">
                <div class="card">
                    <div class="card-content">
                        <h5 class="center-align">LOGIN</h5>
                        @if(count( $errors ) > 0)
                            @foreach ($errors->all() as $error)
                                <p class="error">{{ $error }}</p>
                            @endforeach
                        @endif
                        <form action="{{ url('login') }}" method="post">
                            {{ csrf_field() }}
                            <div class="row">
                                <div class="input-field col s12">
                                    <input id="email" type="email" name="email" class="validate"
                                           value="{{ old('name') }}" required>
                                    <label for="email">email</label>
                                </div>
                            </div>
                            <div class="row">
                                <div class="input-field col s12">
                                    <input id="password" type="password" name="password" class="validate" required>
                                    <label for="password">password</label>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col s12 center-align">
                                    <button type="submit" class="waves-effect waves-light btn blue">Submit</button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
