@extends('layouts.guest')

@section('content')
<div class="container">
    <div class="row">
        <div class="col s6 offset-s3 login-form mt-3">
            <div class="card">
                <div class="card-content">
                    <h5 class="center-align">{{ __('Reset Password') }}</h5>
                    <form action="{{ url('/password/email') }}" method="post">
                        {{ csrf_field() }}
                        <div class="row">
                            <div class="input-field col s12">
                                <input id="email" type="email"  name="email"  class="validate"  value="" required>
                                <label for="email">{{ __('E-Mail Address') }}</label>
                            </div>

                            <div class="col s12">
                                @if ($errors->has('email'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('email') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>
                        <div class="row">
                            <div class="col s12 center-align">
                                <button type="submit" class="waves-effect waves-light btn blue">
                                    {{ __('Send Password Reset Link') }}
                                </button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
