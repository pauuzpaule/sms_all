@extends('layouts.guest')

@section('content')
<div class="container">
    <div class="row">
        <div class="col s6 offset-s3 login-form mt-3">
            <div class="card">
                <div class="card-content">
                    <h5 class="center-align">{{ __('Reset Password') }}</h5>
                    <form action="{{ route('password.update') }}" method="post">
                        {{ csrf_field() }}
                        <div class="row">
                            <div class="input-field col s12">
                                <input id="email" type="email"  name="email"  class="validate"  value="{{ $email ?? old('email') }}" required>
                                <label for="email">Email</label>
                            </div>
                        </div>
                        <div class="row">
                            <div class="input-field col s12">
                                <input id="password" type="password"  name="password"  class="validate"  required>
                                <label for="password">{{ __('Password') }}</label>
                            </div>
                        </div>
                        <div class="row">
                            <div class="input-field col s12">
                                <input id="password" type="password"  name="password_confirmation"  class="validate"  required>
                                <label for="password">
                                    {{ __('Confirm Password') }}
                                </label>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col s12 center-align">
                                <button type="submit" class="waves-effect waves-light btn blue">
                                    {{ __('Reset Password') }}
                                </button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
