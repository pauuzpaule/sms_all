@extends('finance.layouts.app')
@section('content')
<div class="content-holder">
    <div class="row">
        <div class="col s12">
            <h5>
                <a href="{{route("other-fees")}}">Other Fees </a>/ Other Fee Details
            </h5>
            <div class="divider"></div>
        </div>
    </div>
</div>

<div class="row">
    <div class="col s12 ">
        <div class="container">
            <div class="card">
                <div class="card-content">
                    <form action="{{route('save-other-fee')}}" class="form-validate" enctype="multipart/form-data"
                        method="post">
                        {{ csrf_field() }}
                        <input hidden name="id" value="{{!isset($otherFee) ?'': $otherFee->id}}">

                        <div class="row">
                            <div class="input-field col s12">
                                <input id="name" type="text" class="validate"
                                    value="{{!isset($otherFee) ?'': $otherFee->name}}"
                                    name="name" required>
                                <label for="name">Name</label>
                                <div class="error-holder"></div>
                            </div>
                        </div>

                        <div class="row">
                            <div class="input-field col s12">
                                <input id="amount" type="text" class="validate money-format"
                                    value="{{!isset($otherFee) ?'': $otherFee->amount_formatted}}"
                                    name="amount" required>
                                <label for="amount">Amount paid</label>
                                <div class="error-holder"></div>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col s12">
                                <button type="submit" class="waves-effect waves-light btn right">Save</button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection