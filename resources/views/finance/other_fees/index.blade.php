@extends('finance.layouts.app')
@section('content')
<div class="content-holder">
    <div class="row">
        <div class="col s12">
            <h5>Other Fees <a href="{{route('add-other-fee')}}" class="waves-effect waves-light btn btn-small right">+ New </a>
            </h5>
            <div class="divider"></div>
        </div>
    </div>
</div>


<div class="row">
    <div class="col s12">
        <table class="two-row-table mdl-data-table">
            <thead>
                <tr>
                    <th style="widht: 1%">#</th>
                    <th>Name</th>
                    <th>Amount</th>
                    <th></th>
                </tr>
            </thead>

            <tbody>
                @foreach ($otherFees as $key => $otherFee)
                <tr>
                    <td>{{++$key}}</td>
                    <td>{{$otherFee->name}}</td>
                    <td>{{$otherFee->amount_formatted}}</td>
                    <td>
                        <a href="{{url('add-other-fee/'.hashEncode($otherFee->id))}}"
                            class="waves-effect waves-light btn btn-small">Update</a>
                    </td>
                </tr>
                @endforeach
            </tbody>
        </table>
    </div>
</div>
@endsection