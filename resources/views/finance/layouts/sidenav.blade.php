<ul id="slide-out" class="sidenav sidenav-fixed">
    <li>
        <div class="user-view">
            <div class="background">
                <img src="https://materializecss.com/images/office.jpg">
            </div>
            <a href="#user"><img class="circle" src="https://materializecss.com/images/yuna.jpg"></a>
            
            @if (auth()->check())
            <a href="#name"><span class="white-text name">{{auth()->user()->name}}</span></a>
            <a href="#email"><span class="white-text email">{{auth()->user()->email}}</span></a>   
            @else
            <a href="#name"><span class="white-text name">Admin</span></a>
            <a href="#email"><span class="white-text email">admin@admin.com</span></a> 
            @endif
            
        </div>
    </li>
    <li><a href="{{route('fees-structures')}}">Fees Structures <i class="material-icons">school</i></a></li>
    <li><a href="{{route('other-fees')}}">Other Fees <i class="material-icons">school</i></a></li>
    <li><a href="{{route('finance-class-select')}}">Students <i class="material-icons">school</i></a></li>
</ul>