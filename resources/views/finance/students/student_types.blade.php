@extends('layouts.app')
@section('content')
<div class="content-holder">
    <div class="row">
        <div class="col s12">
            <h5>Student Types <a href="{{route('add-student-type')}}" class="waves-effect waves-light btn btn-small right">+ New </a>
            </h5>
            <div class="divider"></div>
        </div>
    </div>
</div>


<div class="row">
    <div class="col s12">
        <table class="two-row-table mdl-data-table">
            <thead>
                <tr>
                    <th style="widht: 1%">#</th>
                    <th>Name</th>
                    <th></th>
                </tr>
            </thead>

            <tbody>
                @foreach ($studentTypes as $key => $studentType)
                <tr>
                    <td>{{++$key}}</td>
                    <td>{{$studentType->name}}</td>
                    <td>
                        <a href="{{url("add-student-type/".hashEncode($studentType->id))}}"
                            class="waves-effect waves-light btn btn-small">Update</a>
                    </td>
                </tr>
                @endforeach
            </tbody>
        </table>
    </div>
</div>
@endsection