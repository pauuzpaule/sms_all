@extends('finance.layouts.app')
@section('content')
<div class="content-holder">
    <div class="row">
        <div class="col s12">
            <h5> <a href="{{route("student-types")}}">Student types</a>/ Student type Details </h5>
            <div class="divider"></div>
        </div>
    </div>
</div>

<div class="row">
    <div class="col s12 ">
        <div class="container">
            <div class="card">
                <div class="card-content">
                    <form class="form-validate" action="{{route('save-student-type')}}" method="post">
                        {{ csrf_field() }}
                        <div class="row">
                            <input type="hidden" name="id" value="{{!isset($studentType) ?'': $studentType->id}}">
                            <div class="input-field col s12">
                                <input id="name" type="text" name="name" class="validate"
                                    value="{{!isset($studentType) ?'': $studentType->name}}" required>
                                <label for="name">Name</label>
                                <div class="error-holder"></div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col s12">
                                <button type="submit" class="waves-effect waves-light btn right">Save</button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection