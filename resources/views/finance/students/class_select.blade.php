@extends('finance.layouts.app')
@section('content')
    <div class="content-holder">
        <div class="row">
            <div class="col s12">
                <h5> <a href="{{route("finance-class-select")}}"> Students </a> </h5>
                <div class="divider"></div>
            </div>
        </div>
    </div>

    <div class="row">
        <div class="col s12 ">
            <div class="container">
                <div class="card">
                    <div class="card-content">
                        <form class="form-validate" action="{{route('finance-all-students')}}" method="post">
                            {{ csrf_field() }}

                            <div class="row">
                                <div class="input-field col s12">
                                    <select name="clazz_id" class="validate" required>
                                        @foreach ($classes as $class)
                                            <option value="{{$class->id}}">{{$class->name}}</option>
                                        @endforeach
                                    </select>
                                    <label for="clazz_id ">Select Class</label>
                                    <div class="error-holder"></div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col s12">
                                    <button type="submit" class="waves-effect waves-light btn right">Continue</button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection