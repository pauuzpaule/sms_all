@extends('finance.layouts.app')
@section('content')
    <div class="content-holder">
        <div class="row">
            <div class="col s12">
                <h5><a href="{{route("finance-class-select")}}"> All Students / </a> [{{$clazz->name}}]</h5>
                <div class="divider"></div>
            </div>
        </div>
    </div>

    <div class="row">
        <div class="col s12 pd-20">
            <table class="data-table mdl-data-table striped">
                <thead>
                <tr>
                    <th>#</th>
                    <th>Image</th>
                    <th>Name</th>
                    <th>Reg No.</th>
                    <th></th>
                </tr>
                </thead>
                <tbody>
                @foreach($students as $key => $student)
                    <tr>
                        <td>{{++$key}}</td>
                        <td>
                            <a href="#!"><img class="circle image-circle-small" src="https://materializecss.com/images/yuna.jpg"></a>
                        </td>
                        <td>{{$student->name}}</td>
                        <td>{{$student->unique_no}}</td>
                        <td>
                            <a href="{{url('/finance/make-payment/'.hashEncode($student->id))}}" class="waves-effect waves-light btn btn-small">Payment</a>
                        </td>
                    </tr>
                @endforeach
                </tbody>
            </table>
        </div>
    </div>
@endsection