<!-- Modal Structure -->
<div id="fees-details-modal" class="modal modal-fixed-footer">
    <div  class="modal-content">
        <h4>Details</h4>
        <h6 v-if="Object.keys(term).length > 0">@{{ term.name }}    [@{{ formatDate(term.start, 'DD/MM/Y') }} - @{{ formatDate(term.end, 'DD/MM/Y') }}]</h6>
        <table>
            <thead>
            <tr>
                <th>#</th>
                <th>Date</th>
                <th>Paid Amount</th>
                <th>Balance</th>
            </tr>
            </thead>
            <tbody>
            <template v-if="payments.paymentDetails">
                <tr v-for="(item, index) in payments.paymentDetails">
                    <td>@{{++index}}</td>
                    <td>@{{ item.date | dateFormat }}</td>
                    <td>@{{ item.amount | toMoney }}</td>
                    <td>@{{ item.balance | toMoney }}</td>
                </tr>
                <tr>
                    <td></td>
                    <td>Total</td>
                    <td>@{{ payments.total | toMoney}}</td>
                    <td>@{{ balance | toMoney}}</td>
                </tr>
            </template>
            </tbody>
        </table>
        <div>
            <h5 class="red-text">Total Fees: @{{ totalFees | toMoney }}</h5>
        </div>
    </div>
    <div class="modal-footer">
        <a href="#!" class="modal-close waves-effect waves-green btn-flat">Ok</a>
    </div>
</div>