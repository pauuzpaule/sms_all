@extends('finance.layouts.app')
@section('content')
    <div class="content-holder">
        <div class="row">
            <div class="col s12">
                <h5> <a href="{{route("finance-class-select")}}"> Students / </a> Payment Infomation </h5>
                <div class="divider"></div>
            </div>
        </div>
    </div>

    <div class="row">
        <div class="col s12 ">
            <div class="container">
                <div class="card">
                    <div class="card-content">
                        <form id="form-make-payment" class="form-validate" action="{{route('save-student-payment')}}" method="post">
                            {{ csrf_field() }}
                            <div class="row">
                                <div class="s12 center-align">
                                    <div>
                                        <a href="#!"><img class="circle image-circle-small" src="https://materializecss.com/images/yuna.jpg"></a>
                                    </div>
                                    <p class="mt">
                                        <span>{{$student->name}}</span><br>
                                        <span>{{$student->unique_no}}</span><br>
                                    </p>
                                </div>
                            </div>

                            <input hidden id="student-id" name="student_id" value="{{$student->id}}">
                            <input hidden id="student-name" value="{{$student->name}}">

                            <div class="row">
                                <div class="input-field col s12">
                                    <input id="amount" type="text" class="validate money-format"
                                           value=""
                                           name="amount" required>
                                    <label for="amount">Amount Paid</label>
                                    <div class="error-holder"></div>
                                </div>
                            </div>

                            <div class="row">
                                <div class="input-field col s12">
                                    <input id="payment_date" type="text" name="payment_date" class="datepicker" value="" required>
                                    <label for="payment_date">Payment Date</label>
                                    <div class="error-holder"></div>
                                </div>
                            </div>

                            <div class="row">
                                <div class="input-field col s12">
                                    <select v-model="paymentTerm" name="term_id" class="validate" required>
                                        <option value="" disabled selected>Choose Term</option>
                                        @foreach ($terms as $term)
                                            <option value="{{$term->id}}">{{$term->name.'   '}} [{{ formatDate($term->start).' - '.formatDate($term->end)}}]</option>
                                        @endforeach
                                    </select>
                                    <label for="clazz_id ">Select Term</label>
                                    <div class="error-holder"></div>
                                </div>
                            </div>

                            <div class="row">
                                <div v-if="isGetPaymentDetails" class="col s12 center-align">
                                    <div class="preloader-wrapper small active">
                                        <div class="spinner-layer spinner-green-only">
                                            <div class="circle-clipper left">
                                                <div class="circle"></div>
                                            </div><div class="gap-patch">
                                                <div class="circle"></div>
                                            </div><div class="circle-clipper right">
                                                <div class="circle"></div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div v-else class="col s12">
                                    <button @click.prevent="confirmPayment" :disabled="isBtnDisabled" type="submit" class="waves-effect waves-light btn right">Confirm Payment</button>
                                </div>

                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>

    @include('finance.students.modal.fees_details')
    @include('error.error_modal')
@endsection