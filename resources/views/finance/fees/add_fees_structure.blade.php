@extends('finance.layouts.app')
@section('content')
<div class="content-holder">
    <div class="row">
        <div class="col s12">
            <h5>
                <a href="{{route("fees-structures")}}">FeesStructures </a>/ FeesStructure Details
            </h5>
            <div class="divider"></div>
        </div>
    </div>
</div>

<div class="row">
    <div class="col s12 ">
        <div class="container">
            <div class="card">
                <div class="card-content">
                    <form action="{{route('save-fees-structure')}}" class="form-validate" enctype="multipart/form-data"
                        method="post">
                        {{ csrf_field() }}
                        <input hidden name="id" value="{{!isset($feesStructure) ?'': $feesStructure->id}}">
                        <div class="row">
                            <div class="input-field col s12">
                                <input id="amount" type="text" class="validate money-format"
                                    value="{{!isset($feesStructure) ?'': $feesStructure->amount_formatted}}"
                                    name="amount" required>
                                <label for="amount">Fees Amount</label>
                                <div class="error-holder"></div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="input-field col s12">
                                <select name="clazz_id" class="validate" required>
                                    @if (isset($feesStructure))
                                    <option value="{{$feesStructure->clazz->id}}">{{$feesStructure->clazz->name}}</option>
                                    @else
                                    <option value="" disabled {{!isset($feesStructure) ? 'selected' : ''}}>Choose Class
                                    </option>
                                    @endif
                                    @foreach ($classes as $class)
                                    <option value="{{$class->id}}">{{$class->name}}</option>
                                    @endforeach
                                </select>
                                <label>Class</label>
                                <div class="error-holder"></div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="input-field col s12">
                                <select name="term_id" class="validate" required>

                                    @if (isset($feesStructure))
                                    <option selected value="{{$feesStructure->term->id}}">{{$feesStructure->term->name.'   '}} [{{ formatDate($feesStructure->term->start).' - '.formatDate($feesStructure->term->end)}}]
                                    </option>
                                    @else
                                    <option value="" disabled {{!isset($feesStructure) ? 'selected' : ''}}>Choose Term
                                    </option>
                                    @endif
                                    @foreach ($terms as $term)
                                    <option value="{{$term->id}}">{{$term->name.'   '}} [{{ formatDate($term->start).' - '.formatDate($term->end)}}]</option>
                                    @endforeach
                                </select>
                                <label>Class</label>
                                <div class="error-holder"></div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="input-field col s12">
                                <select name="student_type_id" class="validate" required>
                                    @if (isset($feesStructure))
                                    <option selected value="{{$feesStructure->studentType->id}}">{{$feesStructure->studentType->name}}
                                    </option>
                                    @else
                                    <option value="" disabled {{!isset($feesStructure) ? 'selected' : ''}}>Choose Student Type
                                    </option>
                                    @endif
                                    @foreach ($studentTypes as $studentType)
                                    <option value="{{$studentType->id}}">{{$studentType->name}}</option>
                                    @endforeach
                                </select>
                                <label>Class</label>
                                <div class="error-holder"></div>
                            </div>
                        </div>

                        @if ($otherFees->count() > 0)
                            <div class="row">
                                <div class="col s12">
                                    <h5>Other Fees</h5>
                                    @foreach ($otherFees as $otherFee)
                                      <p>
                                        <label>
                                          <input type="checkbox" name="otherFees[]" value="{{$otherFee->id}}"
                                                 {{ isset($feesStructure) && in_array($otherFee->name, $feesStructure->otherFees->pluck("name")->toArray()) ? 'checked': ''}} class="filled-in"  />
                                          <span>{{$otherFee->name}}</span>
                                        </label>
                                      </p>
                                    @endforeach
                                    <div class="divider mt-2"></div>
                                </div>
                            </div>
                        @endif

                        <div class="row">
                            <div class="col s12">
                                <button type="submit" class="waves-effect waves-light btn right">Save</button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection