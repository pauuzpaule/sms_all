@extends('finance.layouts.app')
@section('content')
<div class="content-holder">
    <div class="row">
        <div class="col s12">
            <h5>Fees Structures <a href="{{route('add-fees-structure')}}" class="waves-effect waves-light btn btn-small right">+ New </a>
            </h5>
            <div class="divider"></div>
        </div>
    </div>
</div>


<div class="row">
    <div class="col s12">
        <table class="data-table mdl-data-table">
            <thead>
                <tr>
                    <th style="widht: 1%">#</th>
                    <th>Students</th>
                    <th>Class</th>
                    <th>Term</th>
                    <th>Amount</th>
                    <th></th>
                </tr>
            </thead>

            <tbody>
                @foreach ($feesStructures as $key => $feesStructure)
                <tr>
                    <td>{{++$key}}</td>
                    <td>{{$feesStructure->studentType->name}}</td>
                    <td>{{$feesStructure->clazz->name}}</td>
                    <td>{{$feesStructure->term->name}}</td>
                    <td>{{$feesStructure->amount_formatted}}</td>
                    <td>
                        <a href="{{url('add-fees-structure/'.hashEncode($feesStructure->id))}}"
                            class="waves-effect waves-light btn btn-small">Update</a>
                    </td>
                </tr>
                @endforeach
            </tbody>
        </table>
    </div>
</div>
@endsection