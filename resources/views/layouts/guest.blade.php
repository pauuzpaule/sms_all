<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <meta name="base_url" content="{{ route('/') }}">
    <title>Stanbic</title>

    <!-- Fonts -->
    <link href="https://fonts.googleapis.com/css?family=Roboto" rel="stylesheet">
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">

    <link href={{ asset('/css/app.css') }} rel="stylesheet" type="text/css">
</head>

<body>

    <div id="app">

        @include('layouts.guest_nav')
        @yield("content")

    </div>
    <script src={{ asset('/js/app.js') }}></script>
</body>

</html>