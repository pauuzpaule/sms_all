<div class="navbar-fixed">
  <nav class="blue">
    <div class="nav-wrapper">
      <a href="#" class="brand-logo">
        <img style="width: 200px; margin-left: 20px" src="{{asset("img/logo.png")}}" alt="#">
      </a>
      <ul class="right hide-on-med-and-down">
        <li><a href="{{route('/')}}"><i class="material-icons">dashboard</i></a></li>
        <li><a href="{{route('finance')}}"><i class="material-icons">monetization_on</i></a></li>
        <li><a href="{{route('results')}}"><i class="material-icons">chrome_reader_mode</i></a></li>
        <li><a href="#"><i class="material-icons">notifications</i></a></li>
      </ul>
    </div>
  </nav>
</div>