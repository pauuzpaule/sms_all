<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <meta name="base_url" content="{{ route('/') }}">
    <title>SMS</title>

    <!-- Fonts -->
    {{-- <link href="https://fonts.googleapis.com/css?family=Roboto" rel="stylesheet"> --}}
    {{-- <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet"> --}}

    <link href={{ asset('css/app.css') }} rel="stylesheet" type="text/css">
    <link href={{ asset('css/icon.css') }} rel="stylesheet" type="text/css">
</head>

<body>

    <div id="app">
        @include('layouts.nav')
        @include('layouts.sidenav')
        <div class="content">
            @yield("content")
        </div>

        @if (session()->has("show_message"))
            <input id="show-message" hidden value="{{json_encode(session()->get("show_message"))}}">
        @endif
    </div>
    <script src={{ asset('/js/app.js') }}></script>
</body>

</html>