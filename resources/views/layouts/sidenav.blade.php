<ul id="slide-out" class="sidenav sidenav-fixed">
    <li>
        <div class="user-view">
            <div class="background">
                <img src="https://materializecss.com/images/office.jpg">
            </div>
            <a href="#user"><img class="circle" src="https://materializecss.com/images/yuna.jpg"></a>
            
            @if (auth()->check())
            <a href="#name"><span class="white-text name">{{auth()->user()->name}}</span></a>
            <a href="#email"><span class="white-text email">{{auth()->user()->email}}</span></a>   
            @else
            <a href="#name"><span class="white-text name">Admin</span></a>
            <a href="#email"><span class="white-text email">admin@admin.com</span></a> 
            @endif
            
        </div>
    </li>
    <li><a href="{{route('school')}}">School <i class="material-icons">school</i></a></li>
    <li><a href="{{route('terms')}}">Terms <i class="material-icons">school</i></a></li>
    <li><a href="{{route('users')}}">Users <i class="material-icons">people</i></a></li>
    <li><a href="{{route('teachers')}}">Teachers <i class="material-icons">people</i></a></li>
    <li><a href="{{route('classes')}}">Classes <i class="material-icons">class</i></a></li>
    <li><a href="{{route('student-types')}}">Student Types <i class="material-icons">people</i></a></li>
    <li><a href="{{route('students')}}">Students <i class="material-icons">people</i></a></li>
    <li><a href="{{route('subjects')}}">Subjects <i class="material-icons">collections_bookmark</i></a></li>
    <li><a href="{{url("logout")}}">Logout <i class="material-icons">power_settings_new</i></a></li>

    {{-- add more space to bottom --}}
    <div style="height: 150px;"></div>
</ul>