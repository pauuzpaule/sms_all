@extends('results.layouts.app')
@section('content')
    <div class="content-holder">
        <div class="row">
            <div class="col s12">
                <h5>Grades <a href="{{route('add-grade')}}" class="waves-effect waves-light btn btn-small right">+ New </a>
                </h5>
                <div class="divider"></div>
            </div>
        </div>
    </div>

    <div class="container">
        <div class="row">
            <div class="col s12">
                <table class="data-table mdl-data-table">
                    <thead>
                    <tr>
                        <th style="widht: 1%">#</th>
                        <th>Name</th>
                        <th></th>
                    </tr>
                    </thead>

                    <tbody>
                    @foreach ($grades as $key => $grade)
                        <tr>
                            <td>{{++$key}}</td>
                            <td>{{$grade->name}}</td>
                            <td>
                                <a href="{{url('add-grade/'.hashEncode($grade->id))}}"
                                   class="waves-effect waves-light btn btn-small">Update</a>
                                <a href="{{url('add-grade-details/'.hashEncode($grade->id))}}"
                                   class="waves-effect waves-light btn btn-small">Details</a>
                            </td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </div>

@endsection