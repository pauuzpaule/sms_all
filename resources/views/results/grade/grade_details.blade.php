@extends('results.layouts.app')
@section('content')
    <div class="content-holder">
        <div class="row">
            <div class="col s12">
                <h5><a href="{{route('grades')}}">Grades /</a> [{{$grade->name}}]
                </h5>
                <div class="divider"></div>
            </div>
        </div>
    </div>

    <div class="pd-20">
        <div class="row">
            <div class="col s4">
                <form class="form-validate" id="form-save-grade-details" action="#" method="get">
                    <input hidden name="id" :value="gradeDetails.id">
                    <input type="hidden" id="get-grade-id" name="grade_id" value="{{!isset($grade) ?'': $grade->id}}">
                    <div class="row">
                        <div class="input-field col s6">
                            <input id="upper-mark" type="number" name="upper_mark" placeholder="E.g 100"
                                   class="validate"
                                   :value="gradeDetails.upper_mark" required>
                            <label for="upper-mark">Upper Mark</label>
                            <div class="error-holder"></div>
                        </div>
                        <div class="input-field col s6">
                            <input id="lower-mark" type="number" name="lower_mark" placeholder="E.g 80" class="validate"
                                   :value="gradeDetails.lower_mark" required>
                            <label for="lower-mark">Lower Mark</label>
                            <div class="error-holder"></div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="input-field col s6">
                            <input id="symbol" type="text" name="symbol" placeholder="E.g D1,D2,C3 ...F9"
                                   class="validate"
                                   :value="gradeDetails.symbol" required>
                            <label for="symbol">Symbol</label>
                            <div class="error-holder"></div>
                        </div>
                        <div class="input-field col s6">
                            <input id="consist-of" type="number" name="consists_of" placeholder="1,2,3,4,5"
                                   class="validate"
                                   :value="gradeDetails.consists_of" required>
                            <label for="consist-of">Consist Of</label>
                            <div class="error-holder"></div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="input-field col s12">
                            <input id="comment" type="text" name="comment" placeholder="E.g Excellent" class="validate"
                                   :value="gradeDetails.comment">
                            <label for="comment">Comment</label>
                            <div class="error-holder"></div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col s12">
                            <button @click.prevent="saveGradeDetails" type="submit"
                                    class="waves-effect waves-light btn right">Save
                            </button>
                            <button v-if="Object.keys(gradeDetails).length > 0" @click.prevent="clearGradeForm" type="submit"
                                    class="waves-effect waves-light btn right mr mdl-color--red">Clear
                            </button>
                        </div>
                    </div>
                </form>
            </div>
            <div class="col s1"></div>
            <div class="col s7">
                <table class="mdl-data-table">
                    <thead>
                    <tr>
                        <th style="widht: 1%">#</th>
                        <th>Upper</th>
                        <th>Lower</th>
                        <th>Symbol</th>
                        <th>Value</th>
                        <th>Comment</th>
                        <th>Actions</th>
                    </tr>
                    </thead>

                    <tbody>
                    <template>
                        <tr v-for="(item, index) in gradeDetailsList">
                            <td>@{{ index+1 }}</td>
                            <td>@{{ item.upper_mark }}</td>
                            <td>@{{ item.lower_mark }}</td>
                            <td>@{{ item.symbol }}</td>
                            <td>@{{ item.consists_of }}</td>
                            <td>@{{ item.comment }}</td>
                            <td>
                                <a @click="editGradeDetails(index)" href="#">
                                    <i class="material-icons">
                                        edit
                                    </i>
                                </a>
                                <a href="#!">
                                    <i class="material-icons">
                                        delete
                                    </i>
                                </a>
                            </td>
                        </tr>
                    </template>
                    </tbody>
                </table>
            </div>
        </div>
    </div>

@endsection