@extends('results.layouts.app')
@section('content')
    <div class="content-holder">
        <div class="row">
            <div class="col s12">
                <h5> <a href="{{route("exam-sets")}}">Exam Sets</a>/ Exam set Details </h5>
                <div class="divider"></div>
            </div>
        </div>
    </div>

    <div class="row">
        <div class="col s12 ">
            <div class="container">
                <div class="card">
                    <div class="card-content">
                        <form class="form-validate" action="{{route('save-exam-set')}}" method="post">
                            {{ csrf_field() }}
                            <div class="row">
                                <input type="hidden" name="id" value="{{!isset($examSet) ?'': $examSet->id}}">
                                <div class="input-field col s12">
                                    <input id="name" type="text" name="name" class="validate"
                                           value="{{!isset($examSet) ?'': $examSet->name}}" required>
                                    <label for="name">Name</label>
                                    <div class="error-holder"></div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="input-field col s12">
                                    <input id="short-name" type="text" name="short_name" class="validate"
                                           value="{{!isset($examSet) ?'': $examSet->short_name}}" required>
                                    <label for="short-name">Short Name</label>
                                    <div class="error-holder"></div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="input-field col s12">
                                    <input id="total-mark" type="number" name="total_mark" class="validate"
                                           value="{{!isset($examSet) ?'': $examSet->total_mark}}" required>
                                    <label for="total-mark">Total Mark</label>
                                    <div class="error-holder"></div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col s12">
                                    <button type="submit" class="waves-effect waves-light btn right">Save</button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
