@extends('results.layouts.app')
@section('content')
    <div class="content-holder">
        <div class="row">
            <div class="col s12">
                <h5>Exam Sets <a href="{{route('add-exam-set')}}" class="waves-effect waves-light btn btn-small right">+ New </a>
                </h5>
                <div class="divider"></div>
            </div>
        </div>
    </div>

    <div class="row">
        <div class="col s12">
            <table class="data-table mdl-data-table">
                <thead>
                <tr>
                    <th style="widht: 1%">#</th>
                    <th>Name</th>
                    <th>Short Name</th>
                    <th>Total Mark</th>
                    <th></th>
                </tr>
                </thead>

                <tbody>
                @foreach ($examSets as $key => $examSet)
                    <tr>
                        <td>{{++$key}}</td>
                        <td>{{$examSet->name}}</td>
                        <td>{{$examSet->short_name}}</td>
                        <td>{{$examSet->total_mark}}</td>
                        <td>
                            <a href="{{url('add-exam-set/'.hashEncode($examSet->id))}}"
                               class="waves-effect waves-light btn btn-small">Update</a>
                        </td>
                    </tr>
                @endforeach
                </tbody>
            </table>
        </div>
    </div>
@endsection