<!-- Modal Structure -->
<div id="modal-student-no-marks" class="modal modal-fixed-footer">
    <div class="modal-content">
        <h4>Student with no marks</h4>
        <table class="striped bordered">
            <thead>
            <tr>
                <th>NO. </th>
                <th>Name</th>
            </tr>
            </thead>
            <tbody>
            <template v-for='(item, index) in studentsNoMarks'>
               <tr>
                   <td>@{{ item.no }}</td>
                   <td>@{{ item.name }}</td>
               </tr>
            </template>
            </tbody>
        </table>
    </div>
    <div class="modal-footer">
        <a href="#!" @click="submitSaveMarks" class="modal-close waves-effect waves-green btn-flat">Save</a>
        <a href="#!" class="modal-close waves-effect waves-green btn-flat">Cancel</a>
    </div>
</div>