@extends('results.layouts.app')
@section('content')
    <div class="content-holder">
        <div class="row">
            <div class="col s12">
                <h5>Record Results</h5>
                <div class="divider"></div>
            </div>
        </div>
    </div>
    <record-marks route="{{route('view-save-results')}}"></record-marks>
@endsection