@extends('results.layouts.app')
@section('content')
    <div class="content-holder">
        <div class="row">
            <div class="col s12">
                <h5> View Results </h5>
                <div class="divider"></div>
                <form class="form-validate mt" action="{{route('view-results-by-subject')}}" method="post">
                    {{csrf_field()}}
                    <input hidden value="{{json_encode($particulars)}}" id="particulars">
                    <input hidden value="{{$clazz->id}}" name="clazz_id">
                    @if(isset($clazzStream))
                        <input hidden value="{{$clazzStream->id}}" name="clazz_stream_id">
                    @endif
                    <div class="row">
                        <div class="input-field col s3">
                            <select id="exam_set" name="exam_set_id" class="validate" required>
                                <option value="" disabled="" selected>Choose Exam set</option>
                                @foreach($exams as $exam)
                                    <option value="{{$exam->id}}">{{$exam->name}}</option>
                                    @endforeach
                            </select>
                            <label for="exam_set">Exam set</label>
                            <div class="error-holder"></div>
                        </div>
                        <div class="input-field col s3">
                            <select id="term_id" name="term_id" class="validate" required>
                                <option value="" disabled="" selected>Choose Term</option>
                                @foreach($terms as $term)
                                    <option value="{{$term->id}}">{{$term->name}} [{{formatDate($term->start)}}]</option>
                                @endforeach
                            </select>
                            <label for="term_id">Term</label>
                            <div class="error-holder"></div>
                        </div>
                        <div class="input-field col s2">
                            <select id="subject_id"  name="subject_id" class="validate" required>
                                <option value="" disabled="" selected>Choose Subject</option>
                                @foreach($subjects as $subject)
                                    <option value="{{$subject['subject_id']}}">{{$subject['sub_name']}}</option>
                                @endforeach
                            </select>
                            <label for="subject_id">Subjects</label>
                            <div class="error-holder"></div>
                        </div>
                        {{-- <div v-if="selectedParticulars.length > 0" class="input-field col s2">
                            <select id="subject_particular_id" name="subject_particular_id" class="validate" required>
                                <option value="" disabled="" selected>Choose</option>
                                <option v-for="(item, index) in selectedParticulars" :value="item.subject_particular_id">@{{item.sub_pat_name}}</option>
                            </select>
                            <label for="subject_particular_id">Subject Particulars</label>
                            <div class="error-holder"></div>
                        </div> --}}
                        <div class="input-field col s2">
                            <button type="submit" class="waves-effect waves-light btn right">View</button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>

    <div class="row">
        <div class="col s12 pd-20">
            <table class="bordered">
                <thead>
                    <tr>
                        <th>#</th>
                        <th>No.</th>
                        <th>Name</th>
                    </tr>
                </thead>
                <tbody>
                    @foreach($students as $key => $student)
                        <tr>
                            <td>{{++$key}}</td>
                            <td>{{$student->unique_no}}</td>
                            <td>{{$student->name}}</td>
                        </tr>
                        @endforeach
                </tbody>
            </table>
        </div>
    </div>
@endsection
