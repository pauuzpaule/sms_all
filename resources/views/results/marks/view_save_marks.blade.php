@extends('results.layouts.app')
@section('content')
    <input id="data"  type="hidden" value="{{json_encode($data)}}">
    <div class="content-holder">
        <div class="row">
            <div class="col s12">
                <h5>{{$fullMessage}}
                    <a href="#" @click="saveAllMarks" class="waves-effect waves-light btn btn-small right">Save </a>
                </h5>
                <div class="divider"></div>
            </div>
        </div>
    </div>

    <div class="row pd-20">
        <div class="col s12">
            <table class="bordered table-small">
                <thead>
                <tr>
                    <th>#</th>
                    <th>No.</th>
                    <th>Name</th>
                    <th style="width: 12%;">Mark</th>
                </tr>
                </thead>
                <tbody>
                @foreach($students as $key => $student)
                    <tr>
                        <td>{{++$key}}</td>
                        <td>{{$student->unique_no}}</td>
                        <td>{{$student->name}}</td>
                        <td>
                            <input class="marks-input" @input="watchInputMarks" id="{{$student->id}}"
                                   data-id="{{$student->id}}" data-student="{{$student}}" type="number">
                        </td>
                    </tr>
                @endforeach
                </tbody>
            </table>
        </div>
    </div>
    @include('results.marks.modal.save_marks_warning')
@endsection