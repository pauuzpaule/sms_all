@extends('results.layouts.app')
@section('content')
    <div class="content-holder">
        <div class="row">
            <div class="col s12">
                <h5> View Results </h5>
                <div class="divider"></div>
            </div>
        </div>
    </div>

    <div class="row">
        <div class="col s12 ">
            <div class="container">
                <div class="card">
                    <div class="card-content">
                        <form class="form-validate" action="{{route("view-results-details")}}" method="post">
                            {{ csrf_field() }}
                            <class-select size="s12"></class-select>
                            <div class="row">
                                <div class="col s12">
                                    <button type="submit" class="waves-effect waves-light btn right">Continue</button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
