<!-- Modal Structure -->
<div id="modal-error" class="modal modal-fixed-footer">
    <div class="modal-content">
        <h4>Error !</h4>
        <p></p>
    </div>
    <div class="modal-footer">
        <a href="#!" class="modal-close waves-effect waves-green btn-flat">Ok</a>
    </div>
</div>