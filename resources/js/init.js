
$(document).ready(function () {
    $('.sidenav').sidenav();
    $('.modal').modal();
    $('select').formSelect();
    $('.tooltipped').tooltip();

    $('.money-format').maskMoney({
        precision: 0,
      });

    $('.datepicker').datepicker({
        container: 'body',
        format: "yyyy-mm-dd",
        yearRange: 30,
        maxDate: new Date()
    });

    $('.datepicker-any').datepicker({
        container: 'body',
        format: "yyyy-mm-dd",
        yearRange: 30
    });

   
    $('.fixed-action-btn').floatingActionButton();


    $('.data-table').DataTable({
        columnDefs: [{
            targets: [0, 1, 2, 3, 4],
            className: 'mdl-data-table__cell--non-numeric'
        }]
    });

    $('.two-row-table').DataTable({
        columnDefs: [{
            targets: [0, 1],
            className: 'mdl-data-table__cell--non-numeric'
        }]
    });
    $('.three-row-table').DataTable({
        columnDefs: [{
            targets: [0, 1, 2],
            className: 'mdl-data-table__cell--non-numeric'
        }]
    });

    $('.four-row-table').DataTable({
        columnDefs: [{
            targets: [0, 1, 2, 3],
            className: 'mdl-data-table__cell--non-numeric'
        }]
    });


    $(".form-validate").validate({
        ignore: [],
        errorPlacement: function (error, element) {
            var placement = $(element).closest('.input-field').find('.error-holder');
            if (placement) {
                $(placement).append(error)
            } else {
                // error.insertAfter(element);
            }
        }
    })
})