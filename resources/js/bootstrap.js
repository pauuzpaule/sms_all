try {
    window.$ = window.jQuery = require('jquery');
    require("materialize-css")
    

} catch (e) {
    console.log(e)
}

window.app_url = $('meta[name="base_url"]').attr("content");
window.swal = require("sweetalert2");
window.moment = require("moment");
require("jquery-validation");
require("./datatables/datatable.min.js");
require("./datatables/datatable.material.min.js");
require('./money.min.js');
require("./init");
