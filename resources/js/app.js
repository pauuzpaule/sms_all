require('./bootstrap');


import Clazz from "./mixinz/class"
import Teachers from "./mixinz/teachers"
import Students from "./mixinz/students"
import MakePayment from "./mixinz/makePayment"
import GradeDetails from "./mixinz/gradeDetails"
import SaveMarks from "./mixinz/saveMarks"
import ViewResults from "./mixinz/viewResults";


window.Vue = require('vue');

import iziToast from './plugins/izitoast'

Vue.use(iziToast);


Vue.component('class-select', require('./components/ClassSelect.vue'));
Vue.component('image-picker', require('./components/imagePicker.vue'));

Vue.component('class-subjects', require('./components/teacher/ClassSubjects'));
Vue.component('class-teacher-subjects', require('./components/teacher/ClassSelect.vue'));

Vue.component('student-list', require('./components/students/list.vue'));
Vue.component('update-stage-student', require('./components/students/updateStageStudent.vue'));

Vue.component('record-marks', require('./components/marks/recordMarks.vue'));


const app = new Vue({
    el: '#app',
    mixins: [
        Clazz, Teachers, Students, MakePayment, GradeDetails, SaveMarks, ViewResults
    ],
    mounted() {
        let app = this;
        let showMessage = $("#show-message");
        if (showMessage.length > 0) {
            app.showMessage(JSON.parse(showMessage.val()))
        }
    },
    methods: {
        showMessage(objMessage) {
            if (objMessage.withLink) {
                swal.fire({
                    type: objMessage.type,
                    text: objMessage.text,
                }).then((result) => {
                    window.open(objMessage.withLink, '_blank')
                });
                return
            }

            swal.fire({
                type: objMessage.type,
                text: objMessage.text
            })
        },
        toastMessage(message, type = 'success', position = 'bottomRight') {
            let app = this;
            if (type === 'error') {
                app.$iziToast.error({
                    position: position,
                    message: message,
                    progressBar: true,
                    timeout: 5000,
                })
            } else {
                app.$iziToast.success({
                    position: position,
                    message: message,
                    progressBar: true,
                    timeout: 5000,
                })
            }
        },
    }
});
