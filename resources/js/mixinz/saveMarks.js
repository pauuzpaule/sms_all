const saveMarks = {
    data() {
        return {
            studentsNoMarks: [],
            allMarksData: []
        }
    },
    mounted() {
        console.log("Helloooo")
    },
    methods: {
        watchInputMarks(event) {
            let el = event.target;
            let id = el.getAttribute('data-id');
            let value = el.value;

            if (value.length == 0) {
                $("#" + id).val(oldValue);
                return;
            }
            let oldValue = value.substring(0, value.length - 1);
            let newChar = value.slice(-1);

            if (parseInt(value) > 100) {
                $("#" + id).val(oldValue)
            }
        },
        saveAllMarks() {
            let app = this;
            app.studentsNoMarks = [];
            app.allMarksData= [];
            $('.marks-input').each(function (index, item) {
                let data = JSON.parse($("#data").val());
                let student = $(this).data('student');
                data["student_id"] = student.id;
                data["mark"] = $(this).val();
                app.allMarksData.push(data);
                if ($(this).val() == "") {
                    app.studentsNoMarks.push({
                        "no" : student.unique_no,
                        "name": student.name
                    })
                }
                console.log(data)
            });
            //console.log(app.allMarksData);
            if(app.studentsNoMarks.length > 0) {
                $("#modal-student-no-marks").modal("open")
            }else {
                app.submitSaveMarks()
            }
        },
        submitSaveMarks() {
            let app = this;
            $.ajax({
                url: app_url+ "/save-marks",
                data:{
                    marks: app.allMarksData,
                    requestData: JSON.parse($("#data").val())
                },
                success(data) {
                    if(data.error) {
                        swal.fire({
                            type: 'error',
                            text: data.error
                        })
                    }else {
                        swal.fire({
                            type: 'success',
                            text: 'Marks Saved',
                            allowOutsideClick: false
                        }).then(() => {
                            window.location.href = app_url+"/record-results"
                        })
                    }
                    console.log(data)
                }
            })
        }
    }
};

export default saveMarks;