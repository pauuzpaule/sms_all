const gradeDetails = {
    mounted(){
        let app = this;
        let gradeId = $("#get-grade-id");
        if(gradeId.length > 0) {
            app.getGradeDetails(gradeId.val())
        }
    },
    data() {
        return {
            gradeDetailsList : [],
            gradeDetails: {}
        }
    },
    methods : {
        saveGradeDetails(){
            let app = this;
            let form = $("#form-save-grade-details");
            if(form.valid()) {
                $.ajax({
                    url: app_url+ "/save-grade-details",
                    data: form.serialize(),
                    success(data) {
                        console.log(data);
                        if(data.error) {
                            swal.fire({
                                type: 'error',
                                text: data.error
                            })
                        }else {
                            app.toastMessage('Data Saved');
                            app.gradeDetailsList = data.details;
                            app.gradeDetails = {}
                        }
                    }
                })
            }

        },
        editGradeDetails(index) {
            let app = this;
            app.gradeDetails = app.gradeDetailsList[index];
        },
        clearGradeForm() {
            this.gradeDetails = {}
        },
        getGradeDetails(gradeId) {
            let app = this;
            $.ajax({
                url: app_url+ "/get-grade-details",
                data: {
                    gradeId: gradeId
                },
                success(data) {
                    app.gradeDetailsList = data.details;
                }
            })
        }
    }
};
export default gradeDetails;