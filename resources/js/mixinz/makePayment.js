const makePayment = {
    data() {
        return {
            isBtnDisabled : true,
            paymentAmount: "",
            paymentDate: "",
            paymentTerm: "",
            studentId: 0,
            isGetPaymentDetails: false,
            payments: {},
            term: {},
            balance: null,
            totalFees: null,
        }
    },
    methods: {
            confirmPayment() {
                swal.fire({
                    title: 'Are you sure?',
                    html: "You are about to confirm a payment of "+$("#amount").val() +" from "+$("#student-name").val(),
                    type: 'warning',
                    showCancelButton: true,
                    confirmButtonColor: '#3085d6',
                    cancelButtonColor: '#d33',
                    confirmButtonText: 'Yes, confirm payment!'
                }).then((result) => {
                    if (result.value) {
                        $("#form-make-payment").submit()
                    }
                })
            },
            getTermPaymentDetails(termId) {
                let app = this;
                app.isGetPaymentDetails = true;
                app.isBtnDisabled = true;
                $.ajax({
                    url: app_url+ "/finance/term-payment-details",
                    data: {
                        studentId: $("#student-id").val(),
                        termId: termId
                    },
                    success(data) {
                        app.isGetPaymentDetails = false;
                        if(data.error) {
                            swal.fire({
                                type: 'error',
                                title: 'Oops...',
                                text: data.error
                            })
                        }else {
                            $("#fees-details-modal").modal('open');
                            app.payments = data.payments;
                            app.balance = data.balance;
                            app.totalFees = data.totalFees;
                            app.term = data.term;
                            app.isBtnDisabled = false;
                            console.log(data)
                        }

                    }
                })
            },
        formatDate(date, format) {
            return moment(date).format(format);
        }
    },
    watch: {
        paymentTerm: function(val) {
            this.paymentAmount = $("#amount").val();
            this.paymentDate = $("#payment_date").val();
            this.getTermPaymentDetails(val);
        }
    },
    filters: {
        dateFormat(v) {
            return moment(v).format("ddd, DD/MM/Y")
        },
        toMoney(v) {
            if(v ==null) {
                return v;
            }
            let moneyFormat = v.toFixed(2).replace(/\d(?=(\d{3})+\.)/g, '$&,');
            return moneyFormat.substring(0, moneyFormat.length - 3);
        }
    }
};

export default makePayment;
