const teachers  = {
    mounted() {  
    },
    data(){
        return {
            selected_teacher_id: null,
            selected_teacher_name: null
        }
    },
    methods: {
        makeClassTeacher(selectedTeacherId, selectedTeacherName) {
            this.selected_teacher_id = selectedTeacherId
            this.selected_teacher_name = selectedTeacherName
            $('#modal-class-teacher').modal('open');
        },
        submitMakeClassTeacherForm() {
            $("#form-make-class-teacher").submit()
        }
    }
}
export default teachers