const clazz = {
    mounted() {},
    data() {
        return {
            selected_class: null,
            selected_stream: null,
            allSubjects: [],
            classSubjects: [],
            clazz: {},
            saving_stream_subjects: false,
            selectedSubjects: [],
            onlySubjects: [],
            onlySubjectPats: [],
            allClasses: []
        }
    },
    methods: {
        saveSubjects() {
            let app = this
            app.selectedSubjects = []
            $('.is-subject').each((index, item) => {
                if ($(item).is(":checked")) {
                    let subject = $(item).data("subject-id")
                    let subjectPat = $(item).data("subject-pat-id")
                    let subjectObj = {
                        "clazz_id": app.selected_class,
                        "clazz_stream_id": app.selected_stream,
                        "subject_id": subject,
                        "subject_particular_id": subjectPat
                    }
                    app.selectedSubjects.push(subjectObj)
                }
            })

            $.ajax({
                url: app_url + "/save-class-subjects",
                data: {
                    "clazz_id" : app.selected_class,
                    "clazz_stream_id" : app.selected_stream,
                    "subjects": app.selectedSubjects
                },
                success(data) {
                    $('#modal-class-subjects').modal('close');
                    swal.fire({
                        type: 'success',
                        text: 'Subject Data Saved'
                    })
                    console.log(data)
                }
            })
        },
        showSubjects(classId, classUrl, streamId) {
            let app = this;
            app.onlySubjects = []
            app.onlySubjectPats = []
            app.selectedSubjects = []
            $('input:checkbox').prop('checked',false);
            app.selected_class = classId
            app.selected_stream = streamId
            app.saving_stream_subjects = streamId != null
            $.ajax({
                url: app_url + "/get-class-subjects",
                data: {
                    "clazz_id" : app.selected_class,
                    "clazz_stream_id": app.selected_stream
                },
                success(data) {
                    app.allSubjects = data.subjects;
                    app.clazz = data.clazz;
                    app.onlySubjects = data.onlySubjects
                    app.onlySubjectPats = data.onlySubjectPats
                    if(app.clazz.streams && app.clazz.streams.length > 0 && streamId == null) {
                        swal.fire({
                            title: 'Class has streams',
                            text: "Classes with streams add subjects per stream",
                            type: 'warning',
                            showCancelButton: true,
                            confirmButtonColor: '#3085d6',
                            cancelButtonColor: '#d33',
                            confirmButtonText: 'Continue'
                          }).then((result) => {
                            if (result.value) {
                              window.location.href = classUrl
                            }
                          })
                            
                    }else {
                        $('#modal-class-subjects').modal('open');
                    }
                    
                }
            })
        }
    }
};

export default clazz;