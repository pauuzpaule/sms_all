const viewResults = {
    data() {
        return {
            particulars: {},
            selectedParticulars: []
        }
    },
    mounted(){
        let app = this;
        let particularsEl = $("#particulars");
        if(particularsEl.length > 0) {
            app.particulars = JSON.parse(particularsEl.val());
            /*app.particulars.forEach(item => {
                if(item.subject_particular_id != null) {
                    app.selectedParticulars.push(item)
                }
            })*/
        }
    },
    methods: {
        onSubjectSelect() {
            let app = this;
            let value = event.target.value;
            app.selectedParticulars = [];
            let pats = app.particulars[value];
            if(pats.length == 1 && pats[0].subject_particular_id != null) {
                app.selectedParticulars = pats;
            }else if(pats.length > 1) {
                app.selectedParticulars = pats;
            }

            setTimeout(() => {
                $('select').formSelect();
            }, 200)
        }
    }
};

export default viewResults;