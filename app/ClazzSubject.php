<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ClazzSubject extends Model
{
    protected $fillable = [
        "clazz_id", "clazz_stream_id", "subject_id", "subject_particular_id"
    ];

    
}
