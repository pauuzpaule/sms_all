<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class TeacherSubjects extends Model
{
    protected $fillable = [
        "user_id", "clazz_id", "clazz_stream_id", "subject_id", "subject_particular_id"
    ];
}
