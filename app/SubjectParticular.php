<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class SubjectParticular extends Model
{
    protected $fillable = [
        "subject_id", "name", "short_name", "code"
    ];
}
