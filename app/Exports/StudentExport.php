<?php

namespace App\Exports;

use Maatwebsite\Excel\Concerns\Exportable;
use Maatwebsite\Excel\Concerns\WithHeadings;
use Maatwebsite\Excel\Concerns\FromCollection;

class StudentExport implements FromCollection, WithHeadings
{
    use Exportable;
    private $collection;
    private $heading;

    public function __construct($collection, $heading = null)
    {
        $this->collection = $collection;
        $this->heading = $heading;
    }

    public function collection()
    {
        return $this->collection;
    }

    public function headings() : array
    {
        return $this->heading ? $this->heading :  [
            "Name",
            "Date of Birth",
            "Gender",
            "Guardian Name",
            "Guardian Contact",
            "Year of Admission",
            "Errors"
        ];

    }


}
