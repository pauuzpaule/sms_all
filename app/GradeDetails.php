<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class GradeDetails extends Model
{
    protected $fillable = [
        'grade_id', 'upper_mark', 'lower_mark', 'symbol', 'consists_of', 'comment'
    ];

    public function getCommentAttribute($value) {
        return $value ? $value : "No Comment";
    }
}
