<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class FeeStructOtherFees extends Model
{
    protected $fillable = [
        "fees_structure_id", "name", "amount"
    ];
}
