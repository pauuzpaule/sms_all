<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class FeesStructure extends Model
{
    protected $fillable = [
        "student_type_id", "term_id", "clazz_id", "clazz_stream_id", "amount"
    ];

    protected $appends = [
        "amount_formatted"
    ];

    public function getAmountFormattedAttribute()
    {
        return number_format($this->amount);
    }

    public function studentType()
    {
        return $this->belongsTo(StudentType::class);
    }

    public function term()
    {
        return $this->belongsTo(Term::class);
    }

    public function clazz()
    {
        return $this->belongsTo(Clazz::class);
    }

    public function otherFees()
    {
        return $this->hasMany(FeeStructOtherFees::class);
    }
}
