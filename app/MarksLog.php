<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class MarksLog extends Model
{
    protected $fillable = [
        "exam_set_id","term_id", "clazz_id", "clazz_stream_id","subject_id", "subject_particular_id", "mark"
    ];
}
