<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class StudentPayment extends Model
{
    protected $fillable = [
        "student_id", "term_id", "amount", "payment_date"
    ];
}
