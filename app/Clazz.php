<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class Clazz extends Model
{
    protected $fillable = [
        "name", "short_name"
    ];

    public function streams()
    {
        return $this->hasMany(ClazzStream::class);
    }

    public function subjects()
    {
        return $this->hasMany(ClazzSubject::class);
    }

    public function classSubjectDetails()
    {
        return $this->subjects()
            ->select('subjects.name As sub_name', 'subject_particulars.name As sub_pat_name', 'clazz_subjects.*')
            ->join('subjects', 'clazz_subjects.subject_id', '=', 'subjects.id')
            ->leftjoin('subject_particulars', 'clazz_subjects.subject_particular_id', '=', 'subject_particulars.id')
            ->get();
    }

    public function classSubjectList()
    {
        $subjects = $this->classSubjectDetails();
        $groupedSubs = $subjects->map(function ($item, $key) {
            return $item->only(['subject_id', 'sub_name']);
        });
        $pats = $subjects->groupBy(function($item) {
            return $item["subject_id"];
        })->toArray();

        return ["subjects" => $groupedSubs->unique()->values()->all(), "particulars" => $pats];
    }

    public function students()
    {
        return $this->hasMany(Student::class);
    }

    public function feeStructures()
    {
        return $this->hasMany(FeesStructure::class);
    }

}
