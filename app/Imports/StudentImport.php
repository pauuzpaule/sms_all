<?php

namespace App\Imports;

use App\Student;
use App\ModelHelpers\StudentTemp;
use Maatwebsite\Excel\Concerns\ToModel;
use Maatwebsite\Excel\Concerns\Importable;
use Maatwebsite\Excel\Concerns\WithHeadingRow;

class StudentImport implements ToModel, WithHeadingRow
{
    use Importable;

    private $clazz_id;
    private $clazz_stream_id;
    public function __construct($clazz_id = null, $clazz_stream_id = null)
    {
        $this->clazz_id = $clazz_id;
        $this->clazz_stream_id = $clazz_stream_id;
    }

    /**
    * @param array $row
    *
    * @return \Illuminate\Database\Eloquent\Model|null
    */
    public function model(array $row)
    {
        
        if(!$this->isValid($row))
            return;

        return new StudentTemp([
            "clazz_id" => $this->clazz_id,
            "clazz_stream_id" => $this->clazz_stream_id,
            "name" => $row["name"],
            "dob" => $row["date_of_birth"],
            "gender" => $row["gender"],
            "guardian_name" => $row["guardian_name"],
            "guardian_contact" => $row["guardian_contact"],
            "year_of_admin" => $row["year_of_admission"],
        ]);
    }

    private function isValid($row)
    {
        foreach ($row as $key => $value) {
            if($value == null)
                return false;
        }
        return true;
    }
}
