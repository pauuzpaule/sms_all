<?php

namespace App\ModelHelpers;

use App\StudentType;
use Illuminate\Database\Eloquent\Model;

class StudentTemp extends Model
{
    protected $fillable = [
        "clazz_id", "clazz_stream_id", "student_type_id" ,"name", "dob", "year_of_admin", "gender", "guardian_name", "guardian_contact", "image"
    ];

    public function studentType()
    {
        return $this->belongsTo(StudentType::class);
    }
}
