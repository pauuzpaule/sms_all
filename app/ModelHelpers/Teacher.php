<?php
namespace App\ModelHelpers;

use App\User;
use Illuminate\Database\Eloquent\Model;
use App\TeacherSubjects;

class Teacher extends Model {

    protected $table = "users";

    public function subjects()
    {
        return $this->hasMany(TeacherSubjects::class, 'user_id');
    }

    public function subjectsForClazz($clazz_id, $clazz_stream_id = null)
    {
        if(!$clazz_stream_id) {
            return $this->subjects()->where("clazz_id", $clazz_id)->get();
        }

        return $this->subjects()->where("clazz_id", $clazz_id)->where("clazz_stream_id", $clazz_stream_id)->get();
    }
}
