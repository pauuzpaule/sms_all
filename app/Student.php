<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class Student extends Model
{
    protected $fillable = [
        "name", "unique_no", "student_type_id", "dob", "year_of_admin", "image", "gender", "guardian_name", "guardian_contact", "clazz_id", "clazz_stream_id"
    ];

    public function clazz()
    {
        return $this->belongsTo(Clazz::class);
    }

    public function clazzStream()
    {
        return $this->belongsTo(ClazzStream::class);
    }

    public function studentType()
    {
        return $this->belongsTo(StudentType::class);
    }

    public function marks()
    {
        return $this->hasMany(Mark::class);
    }

    public function feeStructure($termId)
    {
        return DB::table('students')
            ->select('students.*', 'fees_structures.id as fees_structure_id', 'fees_structures.amount as school_fees')
            ->join('fees_structures', 'fees_structures.student_type_id', '=', 'students.student_type_id')
            ->where('fees_structures.student_type_id', $this->student_type_id)
            ->where('fees_structures.term_id', $termId)
            ->where('fees_structures.clazz_id', $this->clazz_id)
            ->first();
    }

    public function totalFees($termId)
    {
        $fees = $this->feeStructure($termId);
        if ($fees) {
            $amount = 0;
            $otherFees = FeeStructOtherFees::where("fees_structure_id", $fees->fees_structure_id)->get();
            foreach ($otherFees as $otherFee) {
                $amount += $otherFee->amount;
            }
            return $fees->school_fees + $amount;
        }

        return 0;
    }

    public function feesPayments()
    {
        return $this->hasMany(StudentPayment::class);
    }



    public function clazzDetails()
    {
        $clazz = $this->clazz;
        $clazzStream = $this->clazzStream;
        return (object)[
            "clazz_id" => $clazz->id,
            "clazz_name" => $clazz->name,
            "clazz_stream_id" => isset($clazzStream) ? $clazzStream->id : null,
            "clazz_stream_name" => isset($clazzStream) ? $clazzStream->name : null,
        ];
    }
}
