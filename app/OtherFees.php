<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class OtherFees extends Model
{
    protected $fillable = [
        "name", "amount"
    ];

    protected $appends = [
        "amount_formatted"
    ];

    public function getAmountFormattedAttribute()
    {
        return number_format($this->amount);
    }
}
