<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ReportConfig extends Model
{
    protected $fillable = [
        'clazz_id', 'grade_id', 'position_by', 'points_by', 'advanced_grading', 'do_avg', 'exam_sets'
    ];
}
