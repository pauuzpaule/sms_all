<?php

namespace App\Http\Middleware;

use App\Term;
use Closure;

class TermMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $terms = Term::get();
        if($terms->count() == 0) {
            return redirect('/terms')->with('termMessage', 'Please add term(s) to continue.');
        }
        return $next($request);
    }
}
