<?php

namespace App\Http\Middleware;

use App\School;
use App\Term;
use Closure;

class SchoolMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        //school details available
        $school = School::find(1);
        if(!$school) {
            return redirect('/school-details')->with('schoolMessage', 'Please add school details first.');
        }

        return $next($request);
    }
}
