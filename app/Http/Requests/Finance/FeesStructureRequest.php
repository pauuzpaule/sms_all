<?php

namespace App\Http\Requests\Finance;

use Illuminate\Foundation\Http\FormRequest;
use App\FeesStructure;
use App\OtherFees;
use App\FeeStructOtherFees;

class FeesStructureRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            //
        ];
    }

    public function save()
    {
        $data =  $this->all();
        $data['amount'] = removeCommas($this->amount);
        $feesStructure = FeesStructure::updateOrCreate(["id" => $this->id], $data);
        if($this->has("otherFees")) {
            $array = [];
            FeeStructOtherFees::where("fees_structure_id", $feesStructure->id)->delete();
            foreach ($this->otherFees as $value) {
                $otherFee = OtherFees::find($value);
                $array[] = [
                    "fees_structure_id" => $feesStructure->id,
                    "name" => $otherFee->name,
                    "amount" => $otherFee->amount
                ];
            }
            if (count($array) > 0) {
                FeeStructOtherFees::insert($array);
            }
        }
        return $feesStructure;
    }


    public function saveOtherFee()
    {
        $data =  $this->all();
        $data['amount'] = removeCommas($this->amount);
        return OtherFees::updateOrCreate(["id" => $this->id], $data);
    }
}
