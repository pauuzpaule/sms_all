<?php

namespace App\Http\Requests\Finance;

use App\StudentPayment;
use Illuminate\Foundation\Http\FormRequest;
use App\StudentType;

class StudentRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            //
        ];
    }

    public function saveStudentType()
    {
        return StudentType::updateOrCreate(["id" => $this->id], $this->all());
    }

    public function  savePayment()
    {
        $data = $this->all();
        $data['amount'] = removeCommas($this->amount);
        return StudentPayment::create($data);
    }
}
