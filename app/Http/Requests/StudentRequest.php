<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use App\Student;
use Maatwebsite\Excel\Facades\Excel;
use App\Imports\StudentImport;
use App\ModelHelpers\StudentTemp;
use App\Exports\StudentExport;

class StudentRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        if ($this->route()->uri() == "import-students") {
            return [
                "excel_file" => "required|mimes:xls,xlsx"
            ];
        }

        return [
            //
        ];
    }

    public function saveStudent()
    {
        $data = $this->all();
        if ($this->hasFile('image')) {
            $fileData = storeFile("student/images/", $this->file('image'));
            $data["image"] = $fileData->file_path;
        }
        $student = Student::updateOrCreate(["id" => $this->id], $data);
        $student->update([
            "unique_no" => $this->generateUniqueNo($student)
        ]);
        return $student;
    }

    public function saveStudentTemp()
    {
        $data = $this->all();
        if ($this->hasFile('image')) {
            $fileData = storeFile("student/images/", $this->file('image'));
            $data["image"] = $fileData->file_path;
        }
        $student = StudentTemp::find($this->id);
        $student->update($data);
        return $student;
    }

    private function generateUniqueNo($student)
    {
        if ($student->id < 1000) {
            $str = substr("0000{$student->id}", -4);
        } else {
            $srt = $student->id;
        }
        return substr($student->year_of_admin, 2, 2) . "/s/" . $str;
    }

    public function importStudents()
    {

        $data = (new StudentImport)->toArray($this->file("excel_file"));
        if (!$this->validHeader($data)) {
            dd("Wrong Template file");
        }
        StudentTemp::truncate();
        Excel::import(new StudentImport($this->clazz_id, $this->clazz_stream_id), $this->file("excel_file"));
    }

    private function validHeader($data)
    {
        if (count($data) == 0) {
            return false;
        }

        $correctHeader = [
            "name", "date_of_birth", "gender", "guardian_name", "guardian_contact", "year_of_admission"
        ];
        return array_keys($data[0][0]) === $correctHeader;
    }

    public function confirmImportStudents()
    {
        try {
            
            if(file_exists(storage_path("app/public/student/exports/rejected.xls"))) {
                unlink(storage_path("app/public/student/exports/rejected.xls"));
            }
        } catch (\Throwable $th) {
            //throw $th;
        }
        $studentsTemps = StudentTemp::Has('studentType')->get();
        $rejected = [];
        $acceptedCount = 0;
        foreach ($studentsTemps as $key => $studentTemp) {
            if (count($this->validateImportErrors($studentTemp)) > 0) {
                $errors = $this->validateImportErrors($studentTemp);
                $reject = collect($studentTemp)->put("error", implode(", ", $errors));
                $rejected[] = collect($reject->toArray())->only([
                    "name",
                    "dob",
                    "gender",
                    "guardian_name",
                    "guardian_contact",
                    "year_of_admin",
                    "error"
                ])->toArray();
            } else {
                $student = Student::create($studentTemp->toArray());
                $student->update([
                    "unique_no" => $this->generateUniqueNo($student)
                ]);
                $acceptedCount++;
            }
        }

        if (count($rejected) > 0) {
            (new StudentExport(collect($rejected)))->store("/public/student/exports/rejected.xls");
        }

        StudentTemp::truncate();
        return (object)[
            "acceptedCount" => $acceptedCount,
            "rejectedCount" => count($rejected),
        ];
    }

    private function validateImportErrors(StudentTemp $studentTemp)
    {
        $errors = [];

        if (!isDobValid($studentTemp->dob)) {
            $errors[] = "Date of birth is invalid";
        }

        if (!isGenderValid($studentTemp->gender)) {
            $errors[] = "Gender is invalid, use M or F";
        }

        if (!isYearValid($studentTemp->year_of_admin)) {
            $errors[] = "Year of admission is invalid";
        }

        return $errors;
    }
}

