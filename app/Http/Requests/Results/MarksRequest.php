<?php

namespace App\Http\Requests\Results;

use App\Mark;
use App\MarksLog;
use Illuminate\Foundation\Http\FormRequest;

class MarksRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            //
        ];
    }

    public function saveMarks()
    {
        if($this->checkIfMarksLogged()) {
            return "Marks Already Inserted";
        }else {
            MarksLog::create($this->requestData);
            Mark::insert($this->marks);
            return "SUCCESS";
        }
    }

    private function checkIfMarksLogged()
    {
        $data = $this->requestData;
        $query = MarksLog::query();

        foreach ($data as $key => $value) {
            $query->where($key, (int)$value);
        }

        return $query->first();
    }
}
