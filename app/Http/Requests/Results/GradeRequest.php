<?php

namespace App\Http\Requests\Results;

use App\Grade;
use App\GradeDetails;
use Illuminate\Foundation\Http\FormRequest;

class GradeRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            //
        ];
    }

    public function saveGrade()
    {
        return Grade::updateOrCreate(["id" => $this->id], $this->all());
    }

    public function saveGradeDetails()
    {
        $errors = $this->validateGradeDetails();
        if ($errors != "") {
            return $errors;
        }
        return GradeDetails::updateOrCreate(["id" => $this->id], $this->all());
    }

    private function validateGradeDetails()
    {
        if ($this->lower_mark > $this->upper_mark) {
            return "Lower mark can't be greater than Upper mark";
        }

        return $this->isInRange();
    }

    private function isInRange()
    {
        $grade = Grade::find($this->grade_id);
        $gradeDetail = GradeDetails::find($this->id);
        $details = $grade->details;

        if ($gradeDetail && ($gradeDetail->upper_mark == $this->upper_mark && $gradeDetail->lower_mark == $this->lower_mark)) {
            return "";
        } elseif ($gradeDetail && $gradeDetail->upper_mark != $this->upper_mark) {
            return $this->checkRange($details, $this->upper_mark, null);
        } elseif ($gradeDetail && $gradeDetail->lower_mark != $this->lower_mark) {
            return $this->checkRange($details, null, $this->lower_mark);
        } else {
            return $this->checkRange($details, $this->upper_mark, $this->lower_mark);
        }
    }

    public function checkRange($details, $upperMark = null, $lowerMark = null)
    {
        foreach ($details as $detail) {
            if ($upperMark != null) {
                if ($upperMark >= $detail->lower_mark && $upperMark <= $detail->upper_mark) {
                    return "Upper mark defined in range {$detail->upper_mark} to {$detail->lower_mark}";
                }
            }

            if ($lowerMark != null) {
                if ($lowerMark >= $detail->lower_mark && $lowerMark <= $detail->upper_mark) {
                    return "Lower mark defined in range {$detail->upper_mark} to {$detail->lower_mark}";
                }
            }
        }

        return "";
    }
}
