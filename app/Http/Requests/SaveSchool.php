<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use App\School;

class SaveSchool extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [];
    }

    public function save()
    {
        $data = $this->all();
        if($this->hasFile('badge')) {
            $fileData = storeFile("school/badge/", $this->file('badge'));
            $data["badge"] = $fileData->file_path;
        }
        return School::updateOrCreate(["id" => $this->id], $data);
    }
}
