<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use App\Clazz;
use App\ClazzStream;

class ClassRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            //
        ];
    }

    public function saveClass()
    {
        return Clazz::updateOrCreate(["id" => $this->id], $this->all());
    }

    public function saveStream()
    {
        return ClazzStream::updateOrCreate(["id" => $this->id], $this->all());
    }
}
