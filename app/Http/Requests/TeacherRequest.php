<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use App\TeacherSubjects;
use App\TeacherClasses;

class TeacherRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            //
        ];
    }

    public function saveSubjects()
    {
        TeacherSubjects::where("user_id", $this->user_id)
            ->where("clazz_id", $this->clazz_id)->where("clazz_stream_id", $this->clazz_stream_id)->delete();

        foreach($this->subjects as $subject) {
            TeacherSubjects::create($subject);
        }

        return "done";
    }

    public function makeClassTeacher()
    {
       return  TeacherClasses::updateOrCreate([
            "clazz_id" => $this->clazz_id,
            "clazz_stream_id" => $this->clazz_stream_id,
        ],[
            "clazz_id" => $this->clazz_id,
            "clazz_stream_id" => $this->clazz_stream_id,
            "user_id" => $this->user_id,
        ]);
    }
}
