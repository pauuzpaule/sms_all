<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use App\Term;

class TermRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            //
        ];
    }

    public function saveTerm()
    {
        return Term::updateOrCreate(["id" => $this->id], $this->all());
    }

    public function activateTerm()
    {   
        Term::where("status", "active")->update(["status" => "inactive"]);
        return Term::find(hashDecode($this->term))->update(["status" => "active"]);
    }

}
