<?php

namespace App\Http\Controllers\Finance;

use App\Term;
use App\Clazz;
use App\StudentType;
use App\FeesStructure;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Requests\Finance\FeesStructureRequest;
use App\OtherFees;


class FeesController extends Controller
{
    public function feesStructures()
    {
        $feesStructures = FeesStructure::orderBy("id", "desc")->with('clazz', 'studentType', 'term', 'otherFees')->get();
        return view('finance.fees.fees_structures', compact('feesStructures'));
    }

    public function addFeesStructure($id = null)
    {
        $classes = Clazz::orderBy("id", "desc")->get();
        $terms = Term::orderBy("id", "desc")->get();
        $studentTypes = StudentType::orderBy("name", "desc")->get();
        $otherFees = OtherFees::get();
        if ($id) {
            $feesStructure = FeesStructure::with('clazz', 'studentType', 'term', 'otherFees')
                            ->where("id", hashDecode($id))->first();
            return view('finance.fees.add_fees_structure', compact('feesStructure', 'classes', 'terms', 'studentTypes', 'otherFees'));
        }
        return view('finance.fees.add_fees_structure', compact('classes', 'terms', 'studentTypes', 'otherFees'));
    }

    public function saveFeesStructure(FeesStructureRequest $request)
    {
        $request->save();
        showMessage("success", "Data Saved");
        return redirect()->route('fees-structures');
    }

    public function otherFees()
    {
        $otherFees = OtherFees::orderBy("id", "desc")->get();
        return view('finance.other_fees.index', compact('otherFees'));
    }

    public function addOtherFees($id = null)
    {
        if($id) {
            $otherFee = OtherFees::find(hashDecode($id));
            return view('finance.other_fees.add_other_fees', compact('otherFee'));
        }
        return view('finance.other_fees.add_other_fees');
    }

    public function saveFees(FeesStructureRequest $request)
    {
        $request->saveOtherFee();
        showMessage("success", "Data Saved");
        return redirect()->route('other-fees');
    }
}
