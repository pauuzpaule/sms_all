<?php

namespace App\Http\Controllers\Finance;

use App\Clazz;
use App\FeesStructure;
use App\Student;
use App\StudentType;
use App\Term;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Requests\Finance\StudentRequest as FinanceStudentRequest;


class StudentController extends Controller
{
    public function studentTypes()
    {
        $studentTypes = StudentType::orderBy("id", "desc")->get();
        return view('finance.students.student_types', compact('studentTypes'));
    }

    public function addStudentType($id = null)
    {
        if($id) {
            $studentType = StudentType::find(hashDecode($id));
            return view('finance.students.add_student_type', compact('studentType'));
        }
        return view('finance.students.add_student_type');
    }

    public function saveStudentType(FinanceStudentRequest $studentRequest)
    {
        $studentRequest->saveStudentType();
        showMessage('success', 'Data has been saved');
        return redirect()->route('student-types');
    }

    public function classSelect()
    {
        $classes = Clazz::get();
        $terms = Term::orderBy("id", "desc")->get();
        return view("finance.students.class_select", compact('classes', 'terms'));
    }

    public function allStudents(Request $request)
    {
        $clazz = Clazz::find($request->clazz_id);
        $students = $clazz->students;
        return view("finance.students.all_students", compact('clazz', 'students', 'term'));
    }

    public function makePayment($id)
    {
        $student = Student::find(hashDecode($id));
        $terms = Term::orderBy('id', 'desc')->get();
        return view('finance.students.make_payment', compact('student', 'terms'));
    }
    public function termPaymentDetails(Request $request)
    {
        $student = Student::find($request->studentId);
        $term = Term::find($request->termId);

        //check for fees structure
        $feeStruct = FeesStructure::where("student_type_id", $student->student_type_id)
                        ->where("term_id", $term->id)->where("clazz_id", $student->clazz_id)->first();
        if($feeStruct == null) {
            return response()->json([
                "error" => "Fees Structure for {$student->studentType->name}  Students , {$term->name}, {$student->clazz->name} is not configured"
            ]);
        }

        $totalFees = $student->totalFees($term->id);
        $payments = $this->getTotalPaidForTerm($student, $term->id, $totalFees);
        $balance = $totalFees - $payments['total'];
        return response()->json([
            "student" => $student,
            "term" => $term,
            "totalFees" => $totalFees,
            "payments" => $payments,
            "balance" => $balance,
        ]);
    }

    private function getTotalPaidForTerm(Student $student, $termId, $totalFees)
    {
        $total = 0;
        $paymentsDetails = [];
        $payments = $student->feesPayments()->where("term_id", $termId)->get();
        foreach($payments as $payment) {
            $total += $payment->amount;
            $balance = $totalFees - $total;
            $paymentsDetails[] = [
                "date" => $payment->payment_date,
                "amount" => $payment->amount,
                "balance" => $balance
            ];
        }

        return [
            "total" => $total,
            "paymentDetails" => $paymentsDetails
        ];
    }

    public function saveStudentPayment(FinanceStudentRequest $request)
    {
        $request->savePayment();
        showMessage('success', 'Student Payment has been confirmed');
        return back();
    }

}
