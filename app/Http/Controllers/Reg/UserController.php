<?php

namespace App\Http\Controllers\Reg;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Requests\UserRequest;
use App\User;

class UserController extends Controller
{
    public function index()
    {
        $users = User::orderBy("id", "desc")->get();
        return view("reg.users.index", compact('users'));
    }

    public function addUser($id = null)
    {
        if($id) {
            $user = User::find(hashDecode($id));
            return view("reg.users.add_user", compact('user'));
        }
        return view("reg.users.add_user");
    }

    public function saveUser(UserRequest $userRequest)
    {
        $user = $userRequest->saveUser();
        showMessage("success", "User Data Saved");
        return redirect()->route("users");
    }
}
