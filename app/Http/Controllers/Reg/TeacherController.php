<?php

namespace App\Http\Controllers\Reg;

use App\User;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Clazz;
use App\ClazzStream;
use App\Subject;
use App\Http\Requests\TeacherRequest;
use App\ModelHelpers\Teacher;


class TeacherController extends Controller
{
    public function index()
    {
        $users = User::where("type", "TEACHER")->get();
        return view("reg.teachers.index", compact('users'));
    }

    public function addTeacher()
    {
        $addingTeacher = true;
        return view("reg.users.add_user", compact('addingTeacher'));
    }

    public function addSubjects($id)
    {
        $teacher = Teacher::find(hashDecode($id));
        return view("reg.teachers.add_subjects", compact('teacher'));
    }

    public function getClassSubjects(Request $request)
    {
        $teacher = Teacher::find($request->user_id);
        $teacherSubjects = $teacher->subjectsForClazz($request->clazz_id, $request->clazz_stream_id);

        if ($request->clazz_stream_id != null) {
            $subjects = ClazzStream::find($request->clazz_stream_id)->classSubjectDetails();
        } else {
            $subjects = Clazz::find($request->clazz_id)->classSubjectDetails();
        }

        $onlySubjects = [];
        $onlySubjectPats = [];

        foreach ($teacherSubjects as $subject) {
            $onlySubjects[] = $subject->subject_id;
            if ($subject->subject_particular_id) {
                $onlySubjectPats[] = $subject->subject_particular_id;
            }
        }
        return response()->json([
            "subjects" => $subjects,
            "onlySubs" => $onlySubjects,
            "onlySubPats" => $onlySubjectPats,
            "teacherSubjects" => $teacherSubjects
        ]);
    }

    public function saveSubjects(TeacherRequest $teacherRequest)
    {
        return response()->json($teacherRequest->saveSubjects());
    }

    public function makeClassTeacher(TeacherRequest $teacherRequest)
    {
        $data = $teacherRequest->makeClassTeacher();
        $clazz = Clazz::find($data->clazz_id)->name;
        $user = User::find($data->user_id)->name;
        if($data->clazz_stream_id) {
            $clazzStream = ClazzStream::find($data->clazz_stream_id)->name;
            showMessage("success", "{$user} has been made class teacher of {$clazz} {$clazzStream}");
        }else {
            showMessage("success", "{$user} has been made class teacher of {$clazz}");
        }
        
        return back();
    }
}
