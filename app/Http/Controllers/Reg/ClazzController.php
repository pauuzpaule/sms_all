<?php

namespace App\Http\Controllers\Reg;

use App\Clazz;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Requests\ClassRequest;
use App\ClazzStream;


class ClazzController extends Controller
{
    public function index(Request $request)
    {
        $clazzs = Clazz::orderBy("id", "desc")->with("streams")->get();
        if($request->ajax()) {
            return response()->json($clazzs);
        }
        return view("reg.clazz.index", compact('clazzs'));
    }

    public function addClazz($id = null)
    {
        if($id) {
            $clazz = Clazz::find(hashDecode($id));
            return view("reg.clazz.add_clazz", compact('clazz'));
        }
        return view("reg.clazz.add_clazz");
    }
    public function saveClass(ClassRequest $classRequest)
    {
        $classRequest->saveClass();
        showMessage("success", "Class Data Save");
        return redirect()->route("classes");
    }

    public function streams($id = null)
    {
        if($id) {
            $clazz = Clazz::find(hashDecode($id));
            $streams = $clazz->streams;
            return view("reg.clazz.streams", compact('clazz', 'streams'));
        }
        return back();
    }

    public function addStream($id, $streamId =null)
    {
        $clazz = Clazz::find(hashDecode($id));
        if($streamId) {
            $stream = ClazzStream::find($streamId);
            return view("reg.clazz.add_stream", compact('clazz', 'stream'));
        }
        
        return view("reg.clazz.add_stream", compact('clazz'));
    }

    public function saveStream(ClassRequest $classRequest)
    {
        $stream = $classRequest->saveStream();
        showMessage("success", "Class Stream Data Save");
        return redirect("streams/".hashEncode($stream->clazz_id));
    }
}
