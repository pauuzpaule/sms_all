<?php

namespace App\Http\Controllers\Reg;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Requests\SaveSchool;
use App\School;

class SchoolController extends Controller
{

    public function index()
    {
        $school = School::find(1);
        if(!$school) {
            return redirect()->route("school-details");
        }
        return view("reg.school.details", compact('school'));
    }

    public function details()
    {
        $school = School::find(1);
        if($school) {
            return view("reg.school.index", compact('school'));
        }
        return view("reg.school.index");
    }


    public function saveSchool(SaveSchool $saveSchool)
    {
        $saveSchool->save();
        showMessage("success", "School Data Saved");
        return redirect()->route("school");
    }
}
