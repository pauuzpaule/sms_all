<?php

namespace App\Http\Controllers\Reg;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Requests\SubjectRequest;
use App\Subject;
use App\SubjectParticular;
use App\ClazzSubject;
use App\Clazz;
use App\ClazzStream;

class SubjectController extends Controller
{
    public function index()
    {
        $subjects = Subject::orderBy("name", "desc")->get();
        return view("reg.subjects.index", compact('subjects'));
    }

    public function addSubject($id = null)
    {
        if ($id) {
            $subject = Subject::find(hashDecode($id));
            return view("reg.subjects.add_subject", compact('subject'));
        }
        return view("reg.subjects.add_subject");
    }

    public function saveSubject(SubjectRequest $subjectRequest)
    {
        $subjectRequest->saveSubject();
        showMessage("success", "Subject Data Saved");
        return redirect()->route("subjects");
    }

    public function particulars($id)
    {
        $subject = Subject::find(hashDecode($id));
        $particulars = $subject->particulars;
        return view("reg.subjects.particulars", compact('subject', 'particulars'));
    }

    public function addParticular($id, $particularId = null)
    {
        $id = hashDecode($id);
        if ($particularId) {
            $subjectParticular = SubjectParticular::find($particularId);
            return view("reg.subjects.add_particulars", compact('id', 'subjectParticular'));
        }
        return view("reg.subjects.add_particulars", compact('id'));
    }

    public function saveParticular(SubjectRequest $subjectRequest)
    {
        $particular = $subjectRequest->saveParticular();
        showMessage("success", "Subject Data Received");
        return redirect("particulars/" . hashEncode($particular->subject_id));
    }


    public function getClassSubjects(Request $request)
    {
        $allSubjects = Subject::with('particulars')->get();
        $class = Clazz::where("id", $request->clazz_id)->with("streams")->first();
        
        if ($request->clazz_stream_id != null) {
            $selectedSubjects = ClazzStream::find($request->clazz_stream_id)->subjects;
        } else {
            $selectedSubjects = $class->subjects;
        }

        $onlySubjects = [];
        $onlySubjectPats = [];

        foreach ($selectedSubjects as $subject) {
            $onlySubjects[] = $subject->subject_id;
            if ($subject->subject_particular_id) {
                $onlySubjectPats[] = $subject->subject_particular_id;
            }
        }

        if ($request->ajax()) {
            return response()->json([
                "subjects" => $allSubjects,
                "clazz" => $class,
                "onlySubjects" => $onlySubjects,
                "onlySubjectPats" => $onlySubjectPats
            ]);
        }
        dd($subjects);
    }

    public function saveClassSubjects(Request $request)
    {
        ClazzSubject::where("clazz_id", $request->clazz_id)->where("clazz_stream_id", $request->clazz_stream_id)->delete();
        foreach ($request->subjects as $value) {
            ClazzSubject::create($value);
        }
        return response()->json("Done");
    }
}
