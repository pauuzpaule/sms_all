<?php

namespace App\Http\Controllers\Reg;

use App\Term;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Requests\TermRequest;

class TermController extends Controller
{
    public function index()
    {
        $terms = Term::orderBy("id", "desc")->get();
        return view('reg.term.index', compact('terms'));
    }

    public function addTerm($id=null)
    {
        if($id) {
            $term = Term::find(hashDecode($id));
            return view('reg.term.add_term', compact('term'));
        }
        return view('reg.term.add_term');
    }

    public function saveTerm(TermRequest $termRequest)
    {
        $result = $termRequest->saveTerm();
        return redirect()->route('terms');
    }

    public function activateTerm(TermRequest $termRequest)
    {
        $termRequest->activateTerm();
        return back();
    }
}

