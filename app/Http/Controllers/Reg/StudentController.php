<?php

namespace App\Http\Controllers\Reg;

use App\Clazz;
use App\Student;
use App\StudentType;
use Dompdf\Dompdf;
use App\ClazzStream;
use Illuminate\Http\Request;
use App\Exports\StudentExport;
use App\ModelHelpers\StudentTemp;
use Maatwebsite\Excel\Facades\Excel;
use App\Http\Controllers\Controller;
use App\Http\Requests\StudentRequest;



class StudentController extends Controller
{
    public function index(Request $request)
    {
        $students = [];
        $requestData = [];

        if ($request->has("clazz_stream_id")) {
            $clazz = Clazz::find($request->clazz_id);
            $stream = ClazzStream::find($request->clazz_stream_id);
            $students = $stream->students;
            $requestData["clazz_stream_id"] = $request->clazz_stream_id;
            return view("reg.students.index", compact('students', 'requestData', 'clazz', 'stream'));
        } else if ($request->has("clazz_id")) {
            $clazz = Clazz::find($request->clazz_id);
            $students = $clazz->students;
            $requestData["clazz_id"] = $request->clazz_id;
            return view("reg.students.index", compact('students', 'requestData', 'clazz'));
        }

        return view("reg.students.index", compact('students'));
    }

    public function addStudent($id = null)
    {
        $studentTypes = StudentType::get();
        if($id) {
            $student = Student::with('studentType')->where("id", hashDecode($id))->first();
            return view("reg.students.add_student", compact('student', 'studentTypes'));
         }
        return view("reg.students.add_student", compact('studentTypes'));
    }

    public function exportStudents($clazz, $stream = null)
    {
        $params = [
            "unique_no",
            "name",
            "dob",
            "gender",
            "guardian_name",
            "guardian_contact"
        ];

        $clazz = Clazz::find($clazz);
        $filename = $clazz->name;
        if($stream) {
            $stream = ClazzStream::find($stream);
            $students = $stream->students()->get($params);
            $filename .= " ".$stream->name;
        } else {
            $students = $clazz->students()->get($params);
        }

        $header = [
            "Student No.",
            "Name",
            "Date of birth",
            "Gender",
            "Guardian name",
            "Guardian contact"
        ];
       
        return (new StudentExport(collect($students), $header))->download("{$filename}.xls");
    }

    public function saveStudent(StudentRequest $studentRequest)
    {
        $student = $studentRequest->saveStudent();
        $message = "{$student->name} data has been saved";
        showMessage("success", $message);
        return back();
    }

    public function viewImportStudents()
    {
        return view("reg.students.import.import");
    }

    public function importStudents(StudentRequest $studentRequest)
    {
        $studentRequest->importStudents();
        return redirect()->route("view-stage-students");
    }

    public function viewImportStage()
    {
        $students = StudentTemp::with('studentType')->get();
        return view("reg.students.import.import_stage", compact('students'));
    }

    public function updateImportData($id)
    {
        $studentTypes = StudentType::get();
        $student = StudentTemp::with('studentType')->where("id",$id)->first();
        return view("reg.students.import.add_student", compact("student", "studentTypes"));
    }

    public function saveImportData(StudentRequest $studentRequest)
    {
        $data = $studentRequest->saveStudentTemp();
        showMessage("success", "{$data->name} 's been updated");
        return redirect()->route("view-stage-students");
    }

    public function confirmImportStudents(StudentRequest $studentRequest)
    {
        $result = $studentRequest->confirmImportStudents();
        if ($result->rejectedCount > 0) {
            showMessage(
                "success",
                "{$result->acceptedCount} students have been importred. {$result->rejectedCount} students have been rejected.",
                route("download-rejected-import")
            );
        } else {
            showMessage(
                "success",
                "{$result->acceptedCount} students have been importred."
            );
        }

        return redirect()->route("view-import-students");
    }

    public function downloadRejectedImport()
    {
        return response()->download(storage_path("app/public/student/exports/rejected.xls"));
    }


}
