<?php

namespace App\Http\Controllers\Results;

use App\Grade;
use App\GradeDetails;
use App\Http\Requests\Results\GradeRequest;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class GradeController extends Controller
{
    public function index()
    {
        $grades = Grade::get();
        return view('results.grade.index', compact('grades'));
    }

    public function addGrade($id = null)
    {
        if($id) {
            $grade = Grade::find(hashDecode($id));
            return view('results.grade.add_grade', compact('grade'));
        }
        return view('results.grade.add_grade');
    }

    public function saveGrade(GradeRequest $gradeRequest)
    {
        $gradeRequest->saveGrade();
        showMessage('success', 'Data Saved');
        return redirect()->route('grades');
    }

    public function addGradeDetails($gradeId)
    {
        $grade = Grade::find(hashDecode($gradeId));
        return view('results.grade.grade_details', compact('grade'));
    }

    public function saveGradeDetails(GradeRequest $gradeRequest)
    {
        $gradeDetails = $gradeRequest->saveGradeDetails();
        if($gradeDetails instanceof GradeDetails) {
            $grade = Grade::find($gradeDetails->grade_id);
            $details = $grade->details;
            return response()->json([
                "grade" => $grade,
                "details" => $details
            ]);
        }else {
            return response()->json([
                "error" => $gradeDetails
            ]);
        }

    }

    public function getGradeDetails(Request $request)
    {
        $grade = Grade::find($request->gradeId);
        $details = $grade->details;
        return response()->json([
            "grade" => $grade,
            "details" => $details
        ]);
    }
}
