<?php

namespace App\Http\Controllers\Results;

use App\ExamSet;
use App\Http\Requests\Results\ExamRequest;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class ExamController extends Controller
{
    public function index()
    {
        $examSets = ExamSet::get();
        return view('results.exam.index', compact('examSets'));
    }

    public function addExamSet($id = null)
    {
        if($id) {
            $examSet = ExamSet::find(hashDecode($id));
            return view("results.exam.add_exam_set", compact('examSet'));
        }
        return view("results.exam.add_exam_set");
    }

    public function  saveExamSet(ExamRequest $examRequest)
    {
        $examRequest->saveExamSet();
        showMessage('success', 'Data saved');
        return redirect()->route('exam-sets');
    }
}
