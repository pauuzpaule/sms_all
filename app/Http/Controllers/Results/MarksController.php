<?php

namespace App\Http\Controllers\Results;

use App\Clazz;
use App\ClazzStream;
use App\ExamSet;
use App\Http\Requests\Results\MarksRequest;
use App\Subject;
use App\SubjectParticular;
use App\Term;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class MarksController extends Controller
{
    public function recordResults(Request $request)
    {
        if($request->ajax()) {
            $classes = Clazz::with('streams')->get();
            $exams = ExamSet::get();
            $terms = Term::orderBy("id", "desc")->get();
            return response()->json([
                "classes" => $classes,
                "terms" => $terms,
                "exams" => $exams
            ]);
        }
        return view("results.marks.record_results");
    }

    public function subjectsRecordResult(Request $request)
    {
        if($request->has("clazz_stream_id")) {
            $clazzStream = ClazzStream::find($request->clazz_stream_id);
            $subjects = $clazzStream->classSubjectList();
        }else {
            $clazzStream = Clazz::find($request->clazz_id);
            $subjects = $clazzStream->classSubjectList();
        }

        return response()->json([
            "subjects" => $subjects["subjects"],
            "particulars" => $subjects["particulars"],
        ]);
    }

    public function viewSaveMarks(Request $request)
    {
        $clazz = Clazz::find($request->clazz_id);
        $subject = Subject::find($request->subject_id);
        $clazzMsg = "{$clazz->name} ";
        $subjectMsg = "{$subject->name} ";
        if($request->has('clazz_stream_id')) {
            $clazzStream = ClazzStream::find($request->clazz_stream_id);
            $clazzMsg .= " [$clazzStream->name]";
            $students = $clazzStream->students;
        }else {
            $students = $clazz->students;
        }
        if($request->has('subject_particular_id')) {
            $subPat = SubjectParticular::find($request->subject_particular_id);
            $subjectMsg .= "[$subPat->name]";
        }

        $fullMessage = "Marks - $clazzMsg     $subjectMsg";
        $data = $request->all();
        return view("results.marks.view_save_marks", compact('fullMessage', 'students', 'data'));
    }

    public function saveMark(MarksRequest $marksRequest)
    {
        $result = $marksRequest->saveMarks();
        if($result == 'SUCCESS') {
            return response()->json("DONE");
        }else{
            return response()->json([
                'error' => $result
            ]);
        }
    }

    public function viewResults(Request $request)
    {
        return view("results.marks.view_results");
    }

    public function viewResultsDetails(Request $request)
    {
       
        $clazz = Clazz::find($request->clazz_id);
        $exams = ExamSet::get();
        $terms = Term::orderBy("id", "desc")->get();
        if($request->has('clazz_stream_id')) {
            $clazzStream = ClazzStream::find($request->clazz_stream_id);
            $students = $clazzStream->students;
            $subjectsList = $clazzStream->classSubjectList();
            $subjects = $subjectsList["subjects"];
            $particulars = $subjectsList["particulars"];
            return view("results.marks.view_results_details", compact('students', 'terms', 'exams', 'subjects', 'particulars', 'clazz', 'clazzStream'));
        }else {
            $students = $clazz->students;
            $subjectsList = $clazz->classSubjectList();
            $subjects = $subjectsList["subjects"];
            $particulars = $subjectsList["particulars"];

            return view("results.marks.view_results_details", compact('students', 'terms', 'exams', 'subjects', 'particulars', 'clazz'));
        }
    }

    public function viewResultsBySubject(Request $request)
    {
        $clazz = Clazz::find($request->clazz_id);
        $exams = ExamSet::get();
        $terms = Term::orderBy("id", "desc")->get();
        $examId = $request->exam_set_id;
        $termId = $request->term_id;
        $subjectId = $request->subject_id;

        $selectedSubject = Subject::with('particulars')->where("id", $subjectId)->first();
        
        if($request->has('clazz_stream_id')) {
            $clazzStream = ClazzStream::find($request->clazz_stream_id);
            $students = $clazzStream->students()->with(['marks' => function($q) use ($examId, $termId, $subjectId) {
                $q->where("exam_set_id", $examId)->where("term_id", $termId)->where("subject_id", $subjectId);
           }])->get();
            $subjectsList = $clazzStream->classSubjectList();
            $subjects = $subjectsList["subjects"];
            $particulars = $subjectsList["particulars"];
            return view("results.marks.view_results_with_subject", compact('students', 'terms', 'exams', 'subjects', 'particulars', 'clazz', 'clazzStream', 'selectedSubject'));
        }else {
            $subjectsList = $clazz->classSubjectList();
            $subjects = $subjectsList["subjects"];
            $particulars = $subjectsList["particulars"];
            $students = $clazz->students()->with(['marks' => function($q) use ($examId, $termId, $subjectId) {
                $q->where("exam_set_id", $examId)->where("term_id", $termId)->where("subject_id", $subjectId);
           }])->get();
            return view("results.marks.view_results_with_subject", compact('students', 'terms', 'exams', 'subjects', 'particulars', 'clazz', 'selectedSubject'));
        }
    }
}
