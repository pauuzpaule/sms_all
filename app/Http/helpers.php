<?php

use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\File;
use Vinkla\Hashids\Facades\Hashids;
use Carbon\Carbon;


function formatDate($date, $format="d/M/y")
{
    return Carbon::parse($date)->format($format);
}

function showMessage($type, $text, $link = null)
{
    $content = [
        "type" => $type,
        "text" => $text
    ];
    if ($link) {
        $content["withLink"] = $link;
    }
    session()->flash("show_message", $content);
}

function hashEncode($id)
{
    return Hashids::encode($id);
}

function hashDecode($hashedId)
{
    return Hashids::decode($hashedId)[0];
}

function generateRandomString($length = 6)
{
    $alphabet = '1234567890ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz';
    $id = array();
    $alphaLength = strlen($alphabet) - 1;
    for ($i = 0; $i < $length; $i++) {
        $p = mt_rand(0, $alphaLength);
        $id[] = $alphabet[$p];
    }
    return implode($id);
}

function storeFile($path, $file)
{
    $filename = $path . generateRandomString(25) . '.' . $file->getClientOriginalExtension();
    Storage::disk('public')->put($filename, File::get($file));
    return (object)["filename" => $file->getClientOriginalName(), "file_path" => $filename];
}

function isDobValid($date)
{
    if (!$date)
        return false;

    return preg_match('/^\d{4}-\d{1,2}-\d{1,2}/', $date);
}

function isGenderValid($gender)
{
    if (!$gender)
        return false;

    return strtolower($gender) == "m" || strtolower($gender) == "f";
}

function isYearValid($year)
{
    if (!$year)
        return false;
    return preg_match('/^\d{4}/', $year);
}

function removeCommas($var)
{
    return str_replace(",", "", $var);
}
