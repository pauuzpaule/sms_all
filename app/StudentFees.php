<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class StudentFees extends Model
{
    protected $fillable = [
        "student_id", "fees_structure_id", "term_id", "total_due", "total_balance"
    ];
}
