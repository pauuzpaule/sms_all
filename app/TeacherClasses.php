<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class TeacherClasses extends Model
{
    protected $fillable = [
        "user_id", "clazz_id", "clazz_stream_id"
    ];
}
