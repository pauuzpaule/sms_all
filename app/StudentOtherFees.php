<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class StudentOtherFees extends Model
{
    protected $fillable = [
        "student_id", "term_id", "name", "amount"
    ];
}
