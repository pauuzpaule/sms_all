<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


Auth::routes();

Route::get('logout', '\App\Http\Controllers\Auth\LoginController@logout');
Route::group(['middleware' => ['auth']], function () {


    /*
     |------------------------------------------------------
     | REGISTRATION MODULE
     |-------------------------------------------------------
     | For all the core data used by the system
     */

    //School
    Route::get('/school', 'Reg\SchoolController@index')->name('school');
    Route::get('/school-details', 'Reg\SchoolController@details')->name('school-details');
    Route::post('/save-school', 'Reg\SchoolController@saveSchool')->name('save-school');

    Route::group(['middleware' => 'school'], function () {

        //dashboard
        Route::get('/', 'HomeController@dashboard')->name('/');

        //terms
        Route::get('/terms', 'Reg\TermController@index')->name('terms');
        Route::get('/add-term/{id?}', 'Reg\TermController@addTerm')->name('add-term');
        Route::get('/activate-term', 'Reg\TermController@activateTerm')->name('activate-term');
        Route::post('/save-term', 'Reg\TermController@saveTerm')->name('save-term');

        Route::group(['middleware' => 'term'], function () {

            //Users
            Route::get('/users', 'Reg\UserController@index')->name('users');
            Route::get('/add-user/{id?}', 'Reg\UserController@addUser')->name('add-user');
            Route::post('/save-user', 'Reg\UserController@saveUser')->name('save-user');

            //Clazz
            Route::get('/classes', 'Reg\ClazzController@index')->name('classes');
            Route::get('/add-class/{id?}', 'Reg\ClazzController@addClazz')->name('add-class');
            Route::post('/save-class', 'Reg\ClazzController@saveClass')->name('save-class');
            Route::get('/streams/{id?}', 'Reg\ClazzController@streams')->name('streams');
            Route::get('/add-stream/{id}/{streamId?}', 'Reg\ClazzController@addStream')->name('add-stream');
            Route::post('/save-stream', 'Reg\ClazzController@saveStream')->name('save-stream');

            //subjects
            Route::get('/subjects', 'Reg\SubjectController@index')->name('subjects');
            Route::get('/add-subject/{id?}', 'Reg\SubjectController@addSubject')->name('add-subject');
            Route::post('/save-subject', 'Reg\SubjectController@saveSubject')->name('save-subject');
            Route::get('/particulars/{id}', 'Reg\SubjectController@particulars')->name('particulars');
            Route::get('/add-particular/{id}/{particularId?}', 'Reg\SubjectController@addParticular')->name('add-particular');
            Route::post('/save-particular', 'Reg\SubjectController@saveParticular')->name('save-particular');
            Route::get('/get-class-subjects', 'Reg\SubjectController@getClassSubjects')->name('get-class-subjects');
            Route::get('/save-class-subjects', 'Reg\SubjectController@saveClassSubjects')->name('save-class-subjects');

            //Teachers
            Route::get('/teachers', 'Reg\TeacherController@index')->name('teachers');
            Route::get('/add-teacher', 'Reg\TeacherController@addTeacher')->name('add-teacher');
            Route::get('/add-teacher-subjects/{id}', 'Reg\TeacherController@addSubjects')->name('add-teacher-subjects');
            Route::get('/teacher-get-class-subjects', 'Reg\TeacherController@getClassSubjects')->name('teacher-get-class-subjects');
            Route::get('/teacher-save-subjects', 'Reg\TeacherController@saveSubjects')->name('teacher-save-subjects');
            Route::post('/make-class-teacher', 'Reg\TeacherController@makeClassTeacher')->name('make-class-teacher');

            //Students
            Route::get('/students', 'Reg\StudentController@index')->name("students");
            Route::get('/export-students/{clazz}/{stream?}', 'Reg\StudentController@exportStudents')->name("export-students");
            Route::get('/add-student/{id?}', 'Reg\StudentController@addStudent')->name("add-student");
            Route::post('/save-student', 'Reg\StudentController@saveStudent')->name("save-student");
            Route::get('/view-import-students', 'Reg\StudentController@viewImportStudents')->name("view-import-students");
            Route::get('/view-stage-students', 'Reg\StudentController@viewImportStage')->name("view-stage-students");
            Route::post('/import-students', 'Reg\StudentController@importStudents')->name("import-students");
            Route::get('/view-import-update-students/{id}', 'Reg\StudentController@updateImportData')->name("view-import-update-students");
            Route::post('/save-import-students', 'Reg\StudentController@saveImportData')->name("save-import-students");
            Route::get('/confirm-import-students', 'Reg\StudentController@confirmImportStudents')->name("confirm-import-students");
            Route::get('/download-rejected-import', 'Reg\StudentController@downloadRejectedImport')->name("download-rejected-import");

            Route::get('/test/{mark}', function($clazzId) {
                $clazz = \App\ClazzStream::find($clazzId);
                // dump($clazz->students);
                // dump($clazz->students()->with('marks')->get());
                $examId = 1;
                $termId = 1;
                $subjectId = 2;
                dump($clazz->students()->with(['marks' => function($q) use ($examId, $termId, $subjectId) {
                    $q->where("exam_set_id", $examId)->where("term_id", $termId)->where("subject_id", $subjectId)->where("subject_particular_id", 1);
                }])->get());
            });



            /*
             |------------------------------------------------------
             | FINANCE MODULE
             |-------------------------------------------------------
             | For all the core data used by the system
             */

            Route::get('/finance', 'Finance\FinanceController@index')->name('finance');

            //student
            Route::get('/student-types', 'Finance\StudentController@studentTypes')->name('student-types');
            Route::get('/add-student-type/{id?}', 'Finance\StudentController@addStudentType')->name('add-student-type');
            Route::post('/save-student-type', 'Finance\StudentController@saveStudentType')->name('save-student-type');
            Route::get('/finance/class-select', 'Finance\StudentController@classSelect')->name('finance-class-select');
            Route::post('/finance/all-students', 'Finance\StudentController@allStudents')->name('finance-all-students');
            Route::get('/finance/make-payment/{id}', 'Finance\StudentController@makePayment')->name('finance-make-payment');
            Route::get('/finance/term-payment-details', 'Finance\StudentController@termPaymentDetails')->name('term-payment-details');
            Route::post('/finance/save-student-payment', 'Finance\StudentController@saveStudentPayment')->name('save-student-payment');

            //fees
            Route::get('/fees-structures', 'Finance\FeesController@feesStructures')->name('fees-structures');
            Route::get('/add-fees-structure/{id?}', 'Finance\FeesController@addFeesStructure')->name('add-fees-structure');
            Route::post('/save-fees-structure', 'Finance\FeesController@saveFeesStructure')->name('save-fees-structure');

            Route::get('/other-fees', 'Finance\FeesController@otherFees')->name('other-fees');
            Route::get('/add-other-fee/{id?}', 'Finance\FeesController@addOtherFees')->name('add-other-fee');
            Route::post('/save-other-fee', 'Finance\FeesController@saveFees')->name('save-other-fee');


            /*
            |------------------------------------------------------
            | RESULTS MODULE
            |-------------------------------------------------------
            | For all the core data used by the system
            */

//exams
            Route::get('/results', 'Results\ResultsController@index')->name('results');
            Route::get('/exam-sets', 'Results\ExamController@index')->name('exam-sets');
            Route::get('/add-exam-set/{id?}', 'Results\ExamController@addExamSet')->name('add-exam-set');
            Route::post('/save-exam-set', 'Results\ExamController@saveExamSet')->name('save-exam-set');

//grading
            Route::get('/grades', 'Results\GradeController@index')->name('grades');
            Route::get('/add-grade/{id?}', 'Results\GradeController@addGrade')->name('add-grade');
            Route::post('/save-grade', 'Results\GradeController@saveGrade')->name('save-grade');
            Route::get('/add-grade-details/{id}', 'Results\GradeController@addGradeDetails')->name('add-grade-details');
            Route::get('/save-grade-details', 'Results\GradeController@saveGradeDetails')->name('save-grade-details');
            Route::get('/get-grade-details', 'Results\GradeController@getGradeDetails')->name('get-grade-details');

//results
            Route::get('/record-results', 'Results\MarksController@recordResults')->name('record-results');
            Route::get('/view-save-results', 'Results\MarksController@viewSaveMarks')->name('view-save-results');
            Route::get('/save-marks', 'Results\MarksController@saveMark')->name('save-marks');
            Route::get('/subjects-record-results', 'Results\MarksController@subjectsRecordResult')->name('subjects-record-results');
            Route::get('/view-results', 'Results\MarksController@viewResults')->name('view-results');
            Route::post('/view-results-details', 'Results\MarksController@viewResultsDetails')->name('view-results-details');
            Route::post('/view-results-by-subject', 'Results\MarksController@viewResultsBySubject')->name('view-results-by-subject');

        });

    });
});
