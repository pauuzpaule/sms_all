users
-Admin
-Tech
-Teachers
-students
-Parents
-Finance

Other Entities
-School
-Class
-subject

Processes
-Register users [Admin, Tech, Teachers, Finance]
-Register Students
-Manage Results
-Manage Finance

Table School
-id*
-name*
-motto*
-address*
-website
-badge* [image]

Table users
-id*
-name*                                 
-email*                             
-password*                              
-type*
-gender*
-image
-timestamp [created,updated]
-last_login

Table Teachers Classes [Classes taught by the teacher]
-id*
-user_id*
-class_id*

Table Teachers Subjects [Subjects taught by the teacher]
-id*
-user_id*
-subject_id*
-class_id*
-subject_particular*

Table Clazz
-id*
-name*

Table Clazz Streams
-id*
-clazz_id*
-name*
-short_name*

Table Subject
-id*
-name*
-short_name*

Table Subject Particulars
-id*
-subject_id*
-name*
-code*

Table Clazz Subjects
-id*
-clazz_id*
-subject_id*
-subject_particular_id [nullable]


Table Student 
-id*
-unique_id* [alphaNumeric]
-name*
-clazz_id*
-clazz_stream_id [nullable]
-image*
-dob*
-gender*
-guardian_name*
-guardian_contact*
-year_of_addmission

Table Student Class History
-id*
-student_id*
-clazz_id*
-year*





