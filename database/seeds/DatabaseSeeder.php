<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        // $this->call(UsersTableSeeder::class);

        //create SuperAdmin
        \App\User::create([
            "name" => "Admin Sayrunjah",
            "email" => "admin@admin.com",
            "password" => bcrypt("admin123"),
            "type" => "ADMIN",
            "email_verified_at" => date("Y-m-d H:i:s")
        ]);
    }
}
