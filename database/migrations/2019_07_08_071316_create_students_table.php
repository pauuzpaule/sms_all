<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateStudentsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('students', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('student_type_id');
            $table->string('unique_no')->nullable();
            $table->string('name');
            $table->date("dob");
            $table->string('image')->nullable();
            $table->enum('gender', ["M", "F"]);
            $table->string("guardian_name");
            $table->string("guardian_contact");
            $table->unsignedInteger("clazz_id");
            $table->unsignedInteger("clazz_stream_id")->nullable();
            $table->string("year_of_admin");
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('students');
    }
}
