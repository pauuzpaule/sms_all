<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateReportConfigsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('report_configs', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('clazz_id');
            $table->unsignedInteger('grade_id');
            $table->enum('position_by', ['points', 'marks']);
            $table->enum('points_by', ['asc', 'desc'])->nullable();
            $table->enum('advanced_grading', ['no', 'yes'])->default('no');
            $table->enum('do_avg', ['no', 'yes'])->default('yes');
            $table->string('exam_sets')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('report_configs');
    }
}
