<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMarksLogsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('marks_logs', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('exam_set_id');
            $table->unsignedInteger('term_id');
            $table->unsignedInteger('clazz_id');
            $table->unsignedInteger('clazz_stream_id')->nullable();
            $table->unsignedInteger('subject_id');
            $table->unsignedInteger('subject_particular_id')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('marks_logs');
    }
}
