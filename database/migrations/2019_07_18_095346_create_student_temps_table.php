<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateStudentTempsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('student_temps', function (Blueprint $table) {
            $table->increments('id');
            $table->integer("clazz_id");
            $table->integer("clazz_stream_id")->nullable();
            $table->unsignedInteger('student_type_id')->nullable();
            $table->string('name');
            $table->string("dob");
            $table->string('image')->nullable();
            $table->string('gender');
            $table->string("guardian_name");
            $table->string("guardian_contact");
            $table->string("year_of_admin");
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('student_temps');
    }
}
